# -*- coding: utf-8 -*-
#/usr/bin/python3
'''
###---<SOLAR PHYSICS: Practice 1>---###
@author:Alvaro Rioboo de Larriva
@title:Propagation of magneto-acustic waves under solar spot.
@description: Calculate the propagation length of magneto-acustic waves under a solar spot by the WKB method,
 and present the results in a visual way.
'''
##--<START OF main_s1.py>--##
#-<read parameters from input>-#


#-</read parameters from input>-# #-<define initial parameters>-#


#-</define initial parameters>-#
##-</CORE>--##









##--</CORE>--##
##--<END OF main_s1.py>--##
'''
NOTES:
abcdef
'''
