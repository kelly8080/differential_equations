# -*- coding: utf-8 -*-
#/usr/bin/python3
 #-<general modules>-#
import numpy as np
import matplotlib.pylab as pl
 #-<script modules>-#
from input_module import *
from classes_module import *
from grid_params import *
from functions_module import *
from initial_params import *
 #-<custom modules>-#

'''
###---<SOLAR PHYSICS: Practice 2>---###
@author:Alvaro Rioboo de Larriva
@title: Solution of the potential field starting from fixed ICs.
@description: Build the solution of the potential field starting from ICs and visualize the magnetic field lines in 3D.
'''

##--<START OF main_s1.py>--##
#1. read IO from file: "input_module.py" , "inputs.dat"
#input_handler()
#2. classes to construct "g" and "B": "classes_module.py"
#Grid()
#Magnetic_Field()
#3. set parameters of the grid: "grid_params.py"
#4. define some functions: "functions_module.py"
#magnetic_field()
#algo()
#5. define initial parameters: "initial_params.py"
##-</CORE>--##















##--</CORE>--##
##--<END OF main_s1.py>--##
'''
NOTES:
abcdef
'''
