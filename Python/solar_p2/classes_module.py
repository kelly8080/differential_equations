#-MODULE:classes_module-#
import numpy as np
#2.
#-<define some classes>-#
class Grid():
    '''This is the class to define the GRID of the problem.
    __init__(grid_type,case)//self.{grid_type,case}
    set_limits(x_limits,y_limits)//self.{x0,xf,y0,yf,x_side,y_side,(side)}
    '''
    def __init__(self,grid_type,case):
        self.Grid_type = grid_type
        self.Case = case       
    def set_limits(self,x_limits,y_limits):
        #<limits>
        self.x0 = x_limits[0] ; self.xf = x_limits[1]
        self.y0 = y_limits[0] ; self.yf = y_limits[1]
        #<sides>
        self.x_side = self.xf - self.x0      ; self.y_side = self.yf - self.y0   
        self.side = self.x_side    if (self.x_side==self.y_side) else None #define side if both sides are equal (case=="A" may not have it)
    def set_grid(self,power):
        #<resolution(n_intervals)>
        self.Power = power
        self.Resolution = 2**power
        self.Npoints_1d = self.Resolution + 1
        #<cell>
        self.x_delta = self.x_side/self.Resolution ; self.y_delta = self.y_side/self.Resolution
        self.cell_size = self.x_delta * self.y_delta
        #<meshgrid>
        self.X = np.linspace(self.x0, self.xf, self.Npoints_1d)
        self.Y = np.linspace(self.y0, self.yf, self.Npoints_1d)
        self.MESH = np.meshgrid(self.X, self.Y, sparse=False)
        self.mesh_size = self.x_side * self.y_side
class Magnetic_Field():
    '''
    Class to define the MAGNETIC FIELD (B) that is solution of the potential extrapolation.
    '''
    i = complex(0,1)
    def __init__(self,name):
        self.name == name
    def set_Bx(k_xn, k_mn, *args):
        self.Bx = -i*(k_xn/k_mn)
    def set_By(k_yn, k_mn, *args):
        self.By = -i*(k_yn/k_mn)
    def set_Bz(B0,*args):
        self.B0 = B0
        self.Bz = self.B0  

#-</define some classes>-#
