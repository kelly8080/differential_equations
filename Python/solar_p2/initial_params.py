#-MODULE:initial_params-#

import numpy as np
from functions_module import *
import itertools

#4.
#-<define initial parameters>-#
mm = np.arange(100)
nn = np.arange(100)

kk_xm = wave_number_xm(mm)
kk_yn = wave_number_yn(nn)
kk_mn = (kk_xm**2 + kk_yn**2)


CC_Bz = ContourCondition_Bz(g)
C_mn = custom_FFT(CC_Bz)
B0 = C_mn[0,0]                     #�sto es importante, puesto que es el t�rmino homog�neo de la serie.

def fft_comprobation(C_mn, index=5):
    for i, j in itertools.product(range(index), range(index)): 
        C_real,C_imag = (C_mn[i,j].real,C_mn[i,j].imag) 
        print("[%i,%i]: %.3f+%.3fi"%(i,j,C_real,C_imag)) 
    return
fft_comprobation(C_mn)
#-</define initial parameters>-#

CC_ContourPlot(g.MESH)       #.CC Contour 2D plot function.#


