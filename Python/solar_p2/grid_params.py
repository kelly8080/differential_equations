#-MODULE:grid_params-#

from input_module import *
from classes_module import *

#3.
#-<define grid parameters>-#
power = int(input_handler(5,0))        #"input_module"
nint = 2**power
npx = nint+1

if TASK=="2":
    if CASE=="A":    #rectangle domain with fixed points
        x0,xf = (-1.,1.)    #; x_side = xf-x0
        y0,yf = (-2.,2.)    #; y_side = yf-y0
        
        g = Grid("fixed_rectangle_grid",CASE)
        g.set_limits((x0,xf),(y0,yf))            #
        g.set_grid(power)
        #cleanup local variables
        try: 
            del(x0,xf,y0,yf)
        except:
            pass
    elif CASE=="B":  #square domain: xf-x0=yf-y0
        #start = 2.
        x0,y0 = [float(input_handler(8,i)) for i in range(2)]   #;x0,y0 = (-start,-start) #"input_module"
        side = float(input_handler(7,0))                        #;x_side = y_side = side  #"input_module"
        xf,yf = [i + side for i in (x0,y0)]
        #NOTE: it doesn't say that the square is centered in (0,0). Also,doesn't need to be h/v axed.
        
        g = Grid("squared_grid",CASE)
        g.set_limits((x0,xf),(y0,yf))            #
        g.set_grid(power)
        #cleanup local variables
        try:
            del(side)
            del(x0,xf,y0,yf)
        except:
            pass
    else:
        print("I didn't understand CASE")

#-</define grid parameters>-#
