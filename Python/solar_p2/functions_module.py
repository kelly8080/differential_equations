#-MODULE:functions_module-#

import numpy as np
from math import pi
from grid_params import g      #grid
import matplotlib.pylab as pl  
import pdb

#-<functions>-#
wave_number_xm = lambda m: 2*pi*m /(g.xf-g.x0)
wave_number_yn = lambda n: 2*pi*n /(g.yf-g.y0)


#<1.> CC-Contour Condition (before performing FFT): Bz(x,y,z=0)  (defined by user:)
def ContourCondition_Bz(g,           B0=1):  
    x,y = g.MESH
    x0,y0 = (g.x0, g.y0)
    res = B0*np.sin(x-x0)*np.sin(x-y0)
    #NOTE: x,y are big arrays. Broadcasts with x0,y0
    return res 
#CC_Bz = ContourCondition_Bz(g)

#<2.> Customizable FFT (for the extrapolation)
def custom_FFT(CC_Bz):
    res = np.fft.fft(CC_Bz)
    return res
#C_mn = custom_FFT(CC_Bz)

#<4.> Comprobation of the first coefficients of FFT 
def fft_comprobation(C_mn, index=5):
    for i, j in itertools.product(range(index), range(index)): 
        C_real,C_imag = (C_mn[i,j].real,C_mn[i,j].imag) 
        print("[%i,%i]: %.3f+%.3fi"%(i,j,C_real,C_imag)) 
    return
#fft_comprobation(C_mn)

#<5.> Reconstructed CC from FFT coefficients: Bz(x,y,z=0)
def ContourCondition_reconstructed_Bz(C_mn, g):
    i = complex(0,1)
    k_xm = 1 ; k_yn = 1;
    exp_x = np.exp(i*k_xm* (g.X-g.x0) )
    exp_y = np.exp(i*k_yn* (g.Y-g.y0) )
    return C_mn * exp_x * exp_y
#rec_CC_Bz = ContourCondition_reconstructed_Bz(C_mn)

#<3.> Contour Plot of the Contour Condition
def CC_ContourPlot(mesh,  nlevels=100):
    X,Y = mesh
    Z = ContourCondition_Bz(g)
     
    #-contour_plot-#
    fig = pl.figure(1)
    ax = pl.gca()

    
    cont = ax.contourf(X,Y,Z, levels=nlevels)  #X,Y taken from g.MESH 
    fig.colorbar(cont)
    ax.set_title("Contour plot of CC: $B_z(x,y,z=0)$")
    ax.set_xlabel("X") ; ax.set_ylabel("Y")
    
    
    return
# CC_ContourPlot(g.MESH)



#-</functions>-#
