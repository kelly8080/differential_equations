# Best mood to work with workspaces:
1 screen (MAIN): "Emacs" and "Ipython shell"
1 screen (down): Internet, Documentation
1 screen (left): Practice guide (.pdf)

to:
- switch over workspaces: CTRL+ALT+up/down/left/right
- switch over current programs in the workspace : ALT(keep) + TAB(x2)
- switch to last program used (maybe not in the workspace): ALT + TAB

to(emacs):
- open files: C-x C-f
(to open more than 1 file, you can use *(i.e., *_modules.py)
- save file: C-x C-s
- kill buffer: C-x k 
- next/prev buffer: C-x right/left (aka XF86Forward/Backward)
- quit active command: C-g

