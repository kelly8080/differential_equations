PRO PLOT_SPECIFICS,itt,time
@grid_common

oplot,zz,dblarr(n_elements(zz)),thick=2,linestyle=1 ;TASK3

itt_form  =  string(itt, FORMAT='(I7)') ;11/03
time_form =  string(time, FORMAT='(F7.3)')  ;11/03

xyouts,/norm,0.9,0.91,col=600,'time='+ time_form  ;11/03 tiempo transcurrido
xyouts,/norm,0.9,0.96,col=500,'itt='+ itt_form    ;09/03 numero iteraciones 


end
