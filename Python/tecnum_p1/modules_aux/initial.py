# -*- coding: utf-8 -*-

#-<ROUTINE INITIAL>-#
'''
# PURPOSE:  Calculate the arrays of density, velocity and pressure at time t=0. 
# INPUT ARGUMENTS: they are passed via common block init_common
#         inittype:   char variable with the choice of initial condition
#         shape:   char variable with some subsidiary choice
#                  
# OUTPUT:  the arrays rhoinit, vzinit, presinit (common block)
'''
from math import pi
from numpy import cos,cosh
from scipy.special import gamma



cs00 = (gamm*p00/rho00)**(1./2)  
#--------------------------------------------------------
if (inittype=='sound wave'):
    if (shape=='cosine'):
        # *** YOU HAVE TO WRITE HERE THE PROPER DEFINITIONS ***
	#11/03 GENERAL PARAMETERS	
        #lambd = zf-z0
        kk = 2.*pi/(zf-z0)
        #omega=+cs00*kk
        
        #11/03 FASES DE LA ONDA
	phase = 2.*pi*(zz-z0)/(zf-z0) + 2.*pi/5 #11/03 Define aqui la fase  de la amplitud (depende con z en general)
	add_phase_v = 0.                        #11/03 Define aqui la fase adicional ( =0 MODO w=+cs*k) (= !pi MODO w= -cs*k) ,
                                                #"!dpi es pi en doble precision"
	phase_v = phase + add_phase_v           #11/03 Define aquÃ­ el desfase entre la velocidad y las otras dos fases (iguales). 
	                                        #09/03 function h(z) for 'cosine' 

	#11/03 FUNCION!!
	hh = cos(phase)                    #08/03 a definir en algun common FUNCION!!!
        hh_v = cos(phase_v)        
	
	#09/03 perturbation arrays
	rhoz00 = rho00*ampl*hh
	vz00 = cs00*ampl*hh_v
	presz00 = gamm*p00*ampl*hh
	
	#09/03 initial conditions: ( ECUACIONES (2.3) (las verdaderas cond. iniciales))
	rhoinit = rho00 + rhoz00
        vzinit = v00 + vz00
        presinit = p00 + presz00

        '''
	08/03 "hh" es la funcion que yo defino para cada uno de los 'shapes'
	      NOTE: rhoinit, vinit and presinit contain the *initial condition* 
	            arrays, i.e., the superposition of the equilibrium constants
	            rho00, pres00, vz00 and the perturbation arrays.
        '''

    elif (shape=='gamma_function'):

	#09/03 function h(z) for 'gamma_function'  09/03 TASK 6
	phase = 6.*(zz - (z0+1.2*zf)/2.2)   #11/03 Define aquÃ­ la fase de la amplitud (depende con z en general)
	add_phase_v = 0                   #11/03 Define aquÃ­ la fase adicional (=0 MODO w=+cs*k) (= !pi MODO w=-cs*k), "!dpi es pi en doble precision"
	phase_v = phase + add_phase_v     #11/03 Define aquÃ­ el desfase entre la velocidad y las otras dos fases (iguales)FUNCION!!!
	
		
	#11/03 FUNCION!! hh
	zc = 2.*z0 - 1.3*zf
	hh = gamma(2.*(zz-zc)/(zf-z0))/(10.*cosh(phase)**2)     #(gamma= FUNCION GAMMA), no confundir con parametro gamm
        hh_v = gamma(2.*(zz-zc)/(zf-z0))/(10.*cosh(phase)**2)
	
	#09/03 perturbation arrays :
	rhoz00 = rho00*ampl*hh
        vz00 = cs00*ampl*hh
        presz00 = gamm*p00*ampl*hh
	
	#09/03 initial conditions:
	presinit  = p00   + presz00
	rhoinit = rho00 + rhoz00
        vzinit  = v00  + vz00 
	

    elif (shape=='some_other_shape'):
        print,'routine initial: the some_other_shape shape is not defined yet'
    elif (inittype=='other'):
        print,'routine initial: no other condition is implemented yet'
        
