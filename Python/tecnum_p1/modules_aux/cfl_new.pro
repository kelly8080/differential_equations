FUNCTION cfl_new,rho,momz,cs


; ------------------------------------------------------------------------------------------------;
;												;
; FUNCTION CFL											;
;												;	
;    PURPOSE: Calculate the timestep to guarantee numerical stability				;		
;												;
;    INPUT ARGUMENTS: those necessary to calculate the sound speed, namely			;
;           rho, momz, pres									;
; 												;	
;    COMMON BLOCKS: Note that some necessary input is passed via common				;
;                       blocks (like gamm or the grid zz)					;
;    OUTPUT:  the delta t 									;
; 												;
; ----------------------------------------------------------------------------------------------;

@general_common.pro
@grid_common.pro

;CALCULA "dt" POR LA CONDICION DE COURANT (parametro de Courant cfl)

vcharac1 = abs(momz/rho + cs)    ;08/03 abs(vi+csi) ;08/03 se podria definir directamente en el dt
vcharac2 = abs(momz/rho - cs)    ;08/03 abs(vi-csi) ;08/03 se podria definir directamente en el dt

dt = cfl* (zz[1]-zz[0])/max([vcharac1, vcharac2]) ; 08/03 son uniformes los dt

return,dt

end
