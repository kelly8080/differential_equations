PRO INITIAL

; ----------------------------------------------------------------------
; ROUTINE INITIAL
;
;    PURPOSE:  Calculate the arrays of density, velocity and pressure 
;                 at time t=0. 
;    INPUT ARGUMENTS: they are passed via common block init_common
;              inittype:   char variable with the choice of initial condition
;                 shape:   char variable with some subsidiary choice
;                 
;	** please fill in the list with your choices **
;            
;    COMMON BLOCKS: Note that some additional necessary input is passed via
;                   other common blocks (like the grid parameters,  etc)
;
;    OUTPUT:  the arrays rhoinit, vzinit, presinit (common block)
; ----------------------------------------------------------------------
@general_common
@grid_common
@init_common

;@functions/coseno
;@functions/funcion_gamma

;08/03 MIS_VARIABLES------------------------------------
;NO HACEN FALTA
; 
;SI HACEN FALTA
cs00=(gamm*p00/rho00)^(1d0/2d0)
  
;--------------------------------------------------------
if inittype eq 'sound wave' then begin

    if shape eq 'cosine' then begin
     ; *** YOU HAVE TO WRITE HERE THE PROPER DEFINITIONS ***
	;11/03 GENERAL PARAMETERS	
	lambda=zf-z0	
	kk=2d0*!dpi/(zf-z0)
	;omega=+cs00*kk

        ;11/03 FASES DE LA ONDA
	phase=2d0*!dpi*(zz-z0)/(zf-z0)+2d0*!dpi/5d0 ;11/03 Define aquí la fase  de la amplitud (depende con z en general)
	add_phase_v= 0d0                  ;11/03 Define aquí la fase adicional ( =0d0 MODO w=+cs*k) (= !pi MODO w= -cs*k) , "!dpi es pi en doble precision"
	phase_v= phase  +  add_phase_v        ;11/03 Define aquí el desfase entre la velocidad y las otras dos fases (iguales). 
	;09/03 function h(z) for 'cosine' 

	;11/03 FUNCION!!
	hh=COS(phase) ;08/03 a definir en algun common ;FUNCION!!!
        hh_v=COS(phase_v)        
	
	;09/03 perturbation arrays
	rhoz00=rho00*ampl*hh
	vz00=cs00*ampl*hh_v
	presz00=gamm*p00*ampl*hh
	
	;09/03 initial conditions: ;( ECUACIONES (2.3) (las verdaderas cond. iniciales))
	rhoinit = rho00 +  rhoz00
        vzinit  =  v00  +  vz00
        presinit = p00  +  presz00

	;08/03 "hh" es la funcion que yo defino para cada uno de los 'shapes'
	;      NOTE: rhoinit, vinit and presinit contain the *initial condition* 
	;            arrays, i.e., the superposition of the equilibrium constants
	;            rho00, pres00, vz00 and the perturbation arrays.
    endif

    if shape eq 'gamma_function' then begin

	;09/03 function h(z) for 'gamma_function' ; 09/03 TASK 6
	phase  =  6d0* ( zz-(z0+1.2d0*zf)/2.2d0 )   ;11/03 Define aquí la fase de la amplitud (depende con z en general)
	add_phase_v = 0d0              ;11/03 Define aquí la fase adicional (=0d0 MODO w=+cs*k) (= !pi MODO w=-cs*k), "!dpi es pi en doble precision"
	phase_v=phase + add_phase_v; 11/03 Define aquí el desfase entre la velocidad y las otras dos fases (iguales)FUNCION!!!
	
		
	;11/03 FUNCION!! hh
	zc=2d0*z0-1.3d0*zf
	hh=GAMMA(2d0*(zz-zc)/(zf-z0))/(10*COSH(phase)^2);FUNCION!!!
        hh_v=GAMMA(2d0*(zz-zc)/(zf-z0))/(10*COSH(phase)^2)
	
	;09/03 perturbation arrays :
	rhoz00=rho00*ampl*hh
        vz00=cs00*ampl*hh
        presz00=gamm*p00*ampl*hh
	
	;09/03 initial conditions:
	presinit  = p00   + presz00
	rhoinit = rho00 + rhoz00
        vzinit  = v00  + vz00 
	
    endif

    if shape eq 'some_other_shape' then begin
        print,'routine initial: the some_other_shape shape is not defined yet'
        stop
    endif

   if inittype eq 'other' then begin
        print,'routine initial: no other condition is implemented yet'
        stop
   endif

endif ; 09/03 (if del 'sound wave')

end
