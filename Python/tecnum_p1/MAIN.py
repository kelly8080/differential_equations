#!/usr/bin/python
# -*- coding: utf-8 -*-
#------<PRACTICE 1:DENSITY ADVECTION>------#
#@author:arioboo. Adapted from IDL lenguage.

 #-<general imports>-#
import numpy as np
import os,sys
 #-<additional imports>-#
import scipy as sp
from fractions import Fraction
import pdb
#-<custom imports>-#
def import_tools_modules():
    global mt_draw,mt_diagrams
    
    import modules_tools.draw as mt_draw
    import modules_tools.diagrams as mt_diagrams
def import_aux_modules():
    global ma_fluxes,ma_update,ma_dens2prim

    import modules_aux.fluxes as ma_fluxes
    import modules_aux.update as ma_update
    import modules_aux.dens2prim as ma_dens2prim
 
'''
This is the CORE of the code                                  
It makes decisions and distributes tasks.                     
It also carries out a limited number of assignments itself.   
'''
#-<INITIALIZATION>-#
gamm=5/3              # do not use the name 'gamma' for this variable(to avoid confusion with idl's Euler's Gamma function)
#-<PARAMETER READING (from "inputs.dat")>-#
def read_parameters(filename="./inputs.dat"):
    #1.open "inputs.dat" in read mode
    file = open(filename, "r")
    data = file.readlines()
    def handler(line, index, sep=" "):
        result = data[line].split(sep)[index]
        return result
    #2.read in the parameters for the numerical mesh and create the mesh
    global nint,npz
    global z0,zf
    nint = int(handler(0,0))                         ; npz = nint+2        
    z0,zf = [float(handler(1,i)) for i in range(2)]  

    
    #3.construct the grid of z points following the instructions in the script
    global dz,zz
    dz = (zf-z0)/(npz-1)         
    z_a = z0 - dz/2 ; z_b = zf + dz/2            #puntos primero y ultimo de la malla
    zz = np.arange(npz)/(npz-1)*(z_b-z_a) + z_a  #malla
    
    #4.read in max iterations, max time, output periodicity
    global itmax, timef
    global plotcad, storecad
    itmax = int(handler(2,0))     
    timef = float(handler(3,0)) 
    plotcad, storecad = [int(handler(4,i)) for i in range(2)]
    
    #5.read in the parameters of the initial condition
    global inittype,shape
    inittype = handler(6,0,sep="#").strip()
    shape = handler(7,0,sep="#").strip() 
    
    #6.read in the cfl parameter
    global cfl
    cfl = float(handler(9,0))
    
    #7.read some my_parameters 
    global gamm
    gamm = float(Fraction(handler(10,0)))
    
    #8.read equilibrium values
    global p00,rho00,v00
    global ampl
    p00,rho00,v00 = [float(handler(i,0)) for i in range(12,15)]
    ampl = float(handler(15,0))  #C 
    
    #9.read task to be done
    #readf,unit,form='(a25)',strd    &    task=strtrim(strd,2)

    #10.close read_parameters filename
    file.close()
read_parameters()
pdb.set_trace()
cs00 = (gamm*p00/rho00)**(1./2)# necesario definir aqui para TASK 7
#------------TASK7(Dejar descomentado solo una de las siguientes lineas)--------------
#11/03 TASK7: Descomentar una de estas lineas para la tarea 7. No se pueden leer variables en los "inputs.dat"
#v00=100d0*cs00 #Este valor parece que deja congelada la onda analÃ­tica    
#v00=10d0*cs00
#v00=cs00
#v00=cs00/4d0
#v00=cs00/10d0
#v00=0        #11/03 Recuperamos el caso de la tarea 3
#v00=-cs00/10d0
#v00=-cs00/4d0
#v00=-cs00      # Este valor deja congelada la onda numÃ©rica
#v00=-10d0*cs00
#v00=-100d0*cs00
#-------------------------------
#-</PARAMETERS READING>-#

switch_firsttime=1  # this can serve later on as a switch to carry out certain 
                    # operations the first time round and skip them later. 
itt_fourier=1       #16/03 

#pdb.set_trace()
#-<INITIAL CONDITIONS>-# 
'''This is a call to the routine initial (procedure) that sets up the initial. initial returns the arrays of 
primitive variables: density, velocity pressure, i.e., rhoinit, vzinit, presinit'''

#import_tools_modules()              #importamos los módulos propios
import_aux_modules()

rhoinit, vzinit, presinit = ma_initial()       #call to func
def initial_conditions(rhoinit, vzinit, presinit):
    momzinit = rhoinit*vzinit 
    energinit = presinit/(gamm-1.) + rhoinit*vzinit*vzinit/2. 
    csinit = (gamm*p00/rho00)**(1./2)
    return momzinit,energinit,csinit
pdb.set_trace()                 
momzinit, energinit, csinit = initial_conditions(rhoinit, vzinit, presinit)
#-</INITIAL CONDITIONS>-#   
 
#-<DECLARE/INITIALIZE VARIABLES>- #06/03 variables de "initial.pro"

def initialize_variables():
    vz = vzinit     ;  rho = rhoinit      ; pres = presinit   # 10/03 initialize variables
    momz = momzinit ;  energ = energinit  ; cs = csinit       # 10/03 ""
    rhon = rho      ;  momzn = momz       ; energn = energ    # 10/03 ""
    return vz,rho,pres,momz,energ,cs,rhon,momzn,energn

vz,rho,pres,momz,energ,cs,rhon,momzn,energn = initialize_variables()

itt=0   
time=0 
#-</DECLARE/INITIALIZE VARIABLES>-#

###---<BIG LOOP>---###
while (itt <= itmax) and (time <= timef):   

    #1. CALCULATE FLUXES FROM DENSITIES ('fluxes' is the name of the subroutine) 
    #fluxes,rho,momz, energ, mflz, momflzz, energflz

    #2. TIMESTEP 
    #dt = cfl_new(rho,momz,cs)

    #3. UPDATE ACROSS TIMESTEP using the chosen numerical scheme 
    #update, dt,rho,momz,energ, mflz,momflzz,energflz, rhon,momzn,energn

    #4. BOUNDARY CONDITIONS ('bcs' below is the name of the routine)
    '''
    bcs,'periodic','left',rhon    ; bcs,'periodic','right',rhon  
    bcs,'periodic','left',momzn   ; bcs,'periodic','right',momzn
    bcs,'periodic','left',energn  ; bcs,'periodic','right',energn       
    '''
    #5. CALCULATE PRIMITIVE VARIABLES FROM THE DENSITIES    
    #dens2prim, rhon,momzn,energn, vzn, presn  #10/03
 
		
    #6. EXCHANGE NEW AND OLD VARIABLES
    rho = rhon ; momz = momzn ; energ = energn ; pres = presn ; vz = vzn
    itt += 1         
    time += dt


    #7. STORE RESULTS - to store results in a file from time to time
    if (itt % storecad)==0 :
      '''
      save, filename='./SAVES/variables_itt'+string(itt)+'.sav'
      save, /ROUTINES,filename='./SAVES/routines1.sav' 
      '''
    #8. PLOT RESULTS 
    if (itt % plotcad)==0 or itt==0 or time>=timef:
        draw(rho,pres,vz,time,itt)
       
###---<BIG LOOP>---###

print('program finished.\n','itt=%i \ntime=%.2f \n'%(itt,time))


