###---2a.py---###
# author:arioboo
# Numerical simulations

##--<MODULES>--##
 #-<general modules>-#
from numpy import *
 #-<custom modules>-#
import matplotlib.pylab as pl
from matplotlib.animation import FuncAnimation
import time

##--<INITIAL VARIABLES>--##
 #-<grid_values>-#
potencia = 6
nint = 2**potencia
np = nint+1
 #-<initial_values>-#
a = -1 #velocity (m/s)
 
x0 = -1.5 ; xf = +1.5 ; deltaX = (xf-x0) / (nint) #space (m)
xx = linspace(x0, xf, np)

t0 = 0  ; tf = 10 ; deltaT = 0.98*deltaX / abs(a)  #time (s)

##--<FUNCTIONS>--##
def function(x):
    res = sin(x)**4 *(4*x**3 + 3.5) / cosh(7*x**3) # "a" constant
    #res = ...  # "a" non-constant
    return res

def u(x):  # u in t=t0
    res = function(x)
    return res

def uder_x(u_i1, u_i, dX=deltaX):          # derivative of u over x in x=xi
    res = (u_i1 - u_i) / dX
    return res

def apply_boundaries(u, bound="periodic"):
    if bound=="periodic":
        u = append(u, u[0])  
    return u

def u_update_dt(u_i1, u_i, a, dX=deltaX, dT=deltaT):  # update rule for time derivative
    res = u_i - a*(u_i1 - u_i)*dT/dX
    return res

def uder_t(u_i_updateT, u_i, dT=deltaT):               # derivative of u over t in x=xi,t=t0
    res = (u_i_updateT - u_i)/dT
    return res

def make_plot(xx_num,xx_an,uu_num,uu_an,t):
    #-<plot>-#
    ax1.clear()
    ax1.set_title("Periodic advection wave with 'a' constant : t=%.2f s"%t)
    
    x = [xx_num, xx_an]
    y = [uu_num, uu_an]
    labels = ["numerical","analytical"]
    
    ax1.plot(x[0], y[0], label=labels[0])
    ax1.plot(x[1], y[1], 'g--',label=labels[1])
    
    ax1.legend()
    pl.pause(plot_delay)
    
##--<LOOP>--##
fig = pl.figure(1)
ax1 = fig.add_subplot(1, 1, 1)
ax1.set_ylabel("u(x,t)") ; ax1.set_xlabel("x")
plot_delay = 1e-6

xx0 = xx
uu0 = u(xx0)    # in t = t0

xx_num = xx0
xx_an = xx0     # updated in each it.

uu_num = uu0    # updated in each it.
uu_an = uu0

t = t0
#tf = 0.045
#tf = 0.046
#tf = 0.046*2
tf = 10
it = 0

while t < tf :
    print("t=%.3f s"%t)
    print("it=%i"%it)
    
    
     #-<NUMERICAL solution>-#
    u_i = uu_num[0:np-1] ; u_i1 = uu_num[1:np]       #0. define u in x_i and x_(i+1) from our grid
    u_x = uder_x(u_i1, u_i)                  #1. spacial derivative
    u_i_updateT = u_update_dt(u_i1, u_i, a)  #2. update rule
    u_i_updateT = apply_boundaries(u_i_updateT, "periodic") #3. boundaries 
    u_t = uder_t(u_i_updateT[0:np-1], u_i)           #4. time derivative

 
            #-<ANALYTICAL solution>
    xx_teo = xx_num - a*t
    xx_mod = fmod(xx_teo-x0,xf-x0) + x0                #1. bring them back into domain
    uu_an = u(xx_mod)
    # in python, the modulus operator returns a number that has equal sign that it's dividend. We should use np.fmod
    
    #infinite repetition of initial condition uu0 in (x0,xf)

    make_plot(xx_num,xx_an,uu_num,uu_an,t)
    
     #-<next iteration>-#
    
    uu_num = u_i_updateT ; 
    t += deltaT ;    it += 1                #incremental timesteps
    

