#2b.py
 #-<general modules>-#
import numpy as np
import matplotlib.pylab as pl
 #-<script modules>-#
from math import pi
 #-<custom modules>-#
from matplotlib.animation import FuncAnimation
import time
#from grid_utils import make_grid 

###---<ADVECTION EQUATION>---###
#-<read_inputs>-#
inputs_fname = "inputs.dat"
inputs = open(inputs_fname)

data = inputs.readlines()
def handler(line,index,sep=" "):
    result = data[line].split(sep)[index]
    return result

potencia = int(handler(0,0))
z0,zf = [float(handler(1,i)) for i in range(2)]
t0,tf = [float(handler(2,i)) for i in range(2)]
#a = float(handler(3,0))
def s(zz):
    res = (2*zz - (z0+zf))/(zf-z0)
    return res
def velocity(zz, a0=1./7, b0=8. , W=0.5):
    res = a0 * (1 + b0*np.cos(4*pi*s(zz)/9)**2 * np.exp(-s(zz)**2/W**2))
    return res
                
fdif_type = str(handler(5,0))
#-</read_inputs>-# #-<define parameters>-#
nint = int(2**potencia)
npz = nint + 1

zz = np.linspace(z0, zf, npz)
a = velocity(zz)
                
deltaZ = (zf-z0) / (nint)
deltaT = np.min(0.98*deltaZ / abs(a)) 
plot_delay = 1e-4 #segundos
#-</define parameters>-# #-<FUNCTIONS>-#
def function(x):
    res = np.sin(x)**4 *(4*x**3 + 3.5) / np.cosh(7*x**3) # "a" constant
    #res = ...  # "a" non-constant
    return res

def u(x):  # u in t=t0
    res = function(x)
    return res

def uder_x(u_i1, u_i, fdif_type, dX=deltaZ):          # derivative of u over x in x=xi
    # understanding that u_i1 is the one that is next, and u_i is the one that is previous :
    if fdif_type=="FFD":       
        res = (u_i1 - u_i)/dX
    elif fdif_type=="BFD":
        res = (u_i1 - u_i)/dX
    elif fdif_type=="CFD":
        res = (u_i1 - u_i)/(2*dX)
    else:
        print("fdif_type: Not found")
    
    return res

def apply_boundaries(u, bound="left-periodic"):
    if bound=="right-periodic":  #FFD
        u = np.append(u, u[0])
    elif bound=="left-periodic": #BFD
        u = np.append(u[-1], u)
    elif bound=="center-periodic": #CFD
        u = np.append(u[-1], u)
        u = np.append(u, u[1])
    else:
        print("I didn't understand the boundary")
    return u

def u_update_dt(u_i, u_x, a, dT=deltaT):  # update rule for time derivative
    res = u_i - a*u_x*dT
    return res

def uder_t(u_i_updateT, u_i, dT=deltaT):               # derivative of u over t in x=xi,t=t0
    res = (u_i_updateT - u_i)/dT
    return res

def make_plot(xx_num,xx_an,uu_num,uu_an,t):
    #-<plot>-#
    ax1.clear()
    ax1.set_title("Periodic advection wave with 'a' constant : t=%.2f s"%t)
    
    x = [xx_num, xx_an]
    y = [uu_num, uu_an]
    labels = ["numerical","analytical"]
    
    ax1.plot(x[0], y[0], label=labels[0])
    ax1.plot(x[1], y[1], 'g--',label=labels[1])
    
    ax1.legend(loc=1)
    pl.pause(plot_delay)

#-</FUNCTIONS>-#  #-<LOOP>-#
fig = pl.figure(1)
ax1 = fig.add_subplot(1, 1, 1)
ax1.set_ylabel("u(x,t)") ; ax1.set_xlabel("x")
plot_delay = 1e-6

if fdif_type=="FFD" or fdif_type=="BFD":
    zz0 = zz
if fdif_type=="CFD":
    zz0 = np.linspace(z0, zf, npz)
    deltaZ = (zf-z0) / (nint-1)           ; deltaT = 0.98*deltaZ / abs(a)
    first_out = np.array([z0])            ; first_out = np.array([z0 - deltaZ])    
    last_out = np.array([zf])             #; last_out = np.array([zf + deltaZ])     
    zz0 = np.concatenate( (first_out, zz0), axis=0 )
    zz0[1] = z0                         ; zz0[-1] = zf
    
uu_num = u(zz0)        
uu0 = u(zz0)    # in t = t0

zz_num = zz0
zz_an = zz0     # updated in each it.

uu_num = uu0    # updated in each it.
uu_an = uu0

t = t0
#tf = 0.045
#tf = 0.046
#tf = 0.046*2
tf = 10
it = 0

while t < tf :
    '''
    print("t=%.3f s"%t)
    print("it=%i"%it)
    '''
             ##--<NUMERICAL solution>--##
    if fdif_type=="FFD":
        #'''
        #-<FFD>-#
        u_i = uu_num[0:npz-1] ; u_i1 = uu_num[1:npz]                        #0. define u in x_i and x_(i+1) from our grid
        u_x = uder_x(u_i1, u_i, fdif_type)                                  #1. spacial derivative. (FFD:Forward Finite Differencing)
        u_i_updateT = u_update_dt(u_i, u_x, a[0:npz-1], deltaT)             #2. update rule.        (FFD)
        u_i_updateT = apply_boundaries(u_i_updateT, bound="right-periodic") #3. boundaries.         (FFD)
        u_t = uder_t(u_i_updateT[0:npz-1], u_i)                             #4. time derivative.    (FFD)
        #'''
    elif fdif_type=="BFD":
        #'''
        #-<BFD>-#
        u_i = uu_num[1:npz] ; u_i_1 = uu_num[0:npz-1]                      #0. u_i1->u_i ; u_i->u_i_1
        u_x = uder_x(u_i, u_i_1, fdif_type)                                #1. (BFD:Backward Finite Differencing)
        u_i_updateT = u_update_dt(u_i, u_x, a[1:npz], deltaT)              #2. (BFD)
        u_i_updateT = apply_boundaries(u_i_updateT, bound="left-periodic") #3. (BFD)
        u_t = uder_t(u_i_updateT[1:npz], u_i)                              #4. (BFD)
        #'''
    elif fdif_type=="CFD":
        #'''
        #-<CFD>-#
        u_i1 = uu_num[2::] ; u_i = uu_num[1:-1] ; u_i_1 = uu_num[0:-2]       #0. 
        u_x = uder_x(u_i1, u_i_1, fdif_type, deltaZ)                         #1. (CFD:Centered Finite Differencing)
        u_i_updateT = u_update_dt(u_i, u_x, a, deltaT)                       #2. (CFD)
        u_i_updateT = apply_boundaries(u_i_updateT, bound="center-periodic") #3. (CFD)
        u_t = uder_t(u_i_updateT[1:-1],u_i)                                  #4. (CFD)
        #'''
    else:
        print("fdif_type: Not defined")
        
            ##--<ANALYTICAL solution>--##
    zz_teo = zz_num - a*t
    zz_mod = np.fmod(zz_teo-z0,zf-z0) + z0                        #1. bring them back into domain
    #zz_mod = np.mod(zz_teo+zf,zf+zf) -zf
    uu_an = u(zz_mod)
    # in python, the modulus operator returns a number that has equal sign that it's dividend. We should use np.fmod
    
    #infinite repetition of initial condition uu0 in (x0,xf)

    make_plot(zz_num,zz_an,uu_num,uu_an,t)
    
     #-<next iteration>-#
    
    uu_num = u_i_updateT ; 
    t += deltaT ;    it += 1                #incremental timesteps

      
###---<END>---###
