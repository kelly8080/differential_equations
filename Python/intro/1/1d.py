#1d.py
#@author:arioboo
#-<ADVECTION EQUATION>-#
import numpy as np
import matplotlib.pylab as pl
from math import pi



u = lambda x: np.sin(x)

xx = np.linspace(0,2*pi,100)
uu_0 = u(xx)

##---<LOOP>---###
fig,ax = pl.subplots()         ; ax.plot(xx,uu_0)  # Initial condition       
fig1,ax1 = pl.subplots()

a = -1
deltaT = 1  #segundos
t0 = 0 ; tf = 10
t = t0 ; it = 0
while t < tf:
    xx_after = xx - a*t
    uu_after = u(xx_after)
    print("t = %.1fs"%t , ";", "it = %i"%it )
    t += deltaT ; it += 1
    #-plots
    ax.set_title("Periodic function over $t$")
    ax.set_xlabel("$x$") ; ax.set_ylabel("$sin(x)$")
    ax.plot(xx_after,uu_after) # Solution after t 
    fig.show()

    ax1.set_title("Spacetime points for $u(x,t) = u(x_0,0)$")
    ax1.set_xlabel("$x$") ; ax1.set_ylabel("$x-a*t$")
    ax1.plot(xx,xx_after)       # X-T space 
    fig1.show()
##---</LOOP>---###


t = np.linspace(0,10,1000)
