###---1a.py---###
# author:arioboo
# Numerical simulations
#-------------------------------
 #-<general_modules>-#
import numpy as np
 #-<custom_modules>-#
import matplotlib.pylab as pl


#----------------------------------------------
potencia = 6         # POTENCIA DEL N DE INTERVALOS  :: probar 4,5,6,7,8,9,10
nint = 2**potencia    # N DE INTERVALOS
npx = nint+1         # N DE PUNTOS (nint+1)
#----------------------------------------------

x0 = -1.5   # VALOR INICIAL 
xf = 6      # VALOR FINAL

xx = np.linspace(xf, x0, npx)  # SAMPLEADO DE PUNTOS (MALLA)
DELTAx = xx[1]-xx[0]      # INTERVALO UNIFORME
#----------------FUNCION_ANALITICA (en los puntos i)----------------------------
hh_an = 2 * (1 + np.tanh(xx)) * np.sin(xx) * np.cosh((xx+2)/2)**(-1)  #FUNCION ANALITICA en los puntos i

hp_an = np.cosh((2+xx)/2)*(2*np.cosh(xx)**(-2)*np.sin(xx)  +  (1+np.tanh(xx))*(2*np.cosh(xx)-np.sin(xx)*np.tanh((2+xx)/2)) ) #FUNCION ANALITICA en los puntos i

#FUNCION_ANALITICA (en los puntos i+1/2)
xx_12 = np.linspace(xf,x0,npx-1) + DELTAx/2
xx_12 = xx_12[0:-2]  #baja uno la long. del vector

hp_an_12 = np.cosh((2+xx_12)/2) *(2*np.cosh(xx_12)**(-2)*np.sin(xx_12)  +  (1+np.tanh(xx_12))*(2*np.cosh(xx_12)-np.sin(xx_12)*np.tanh((2+xx_12)/2))) #FUNCION ANALITICA (en los puntos i+1/2)
#--------------FUNCION_NUMERICA----------------------------------------
hp_num = (hh_an[1:-1]-hh_an[0:-2])/(xx[1:-1]-xx[0:-2])
# Recordar que hp_an(i) aprox. hp_num(i+1/2)


#----------------plots-----------
task = '1a'    #'1a','1c'


def make_plot(x,y,clear_frame=True):
    if clear_frame:
        pl.clf()               # creates a frame if doesn't exist and clear a frame
    pl.plot(x,y)
    #pl.savefig("hp_an")    # + ".png" by default
    
'''
pl.clf()
make_plot(xx,hh_an,False)
make_plot(xx,hp_an,False)
make_plot(xx_12,hp_an_12,False)
make_plot(xx[1:-1]-xx[0:-2],hp_num,False)
'''
#-------------------------------------------------------
