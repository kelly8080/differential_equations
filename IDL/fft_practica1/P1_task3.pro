pro P1_task3, vz,rho,pres, itt,time

	@grid_common
	@init_common
	@general_common

	;CALCULOS--------------
	cs=(gamm*pres/rho)^(1d0/2d0)	
	cs00=(gamm*p00/rho00)^(1d0/2d0) ; velocidad del sonido
	;FUNCION_ANALITICA(para 'cosine')----
	lambda=zf-z0        ; periódica
	kk=2d0*!dpi/lambda ; periódica
	omega= cs00*kk     ;Esto viene de la resolucion de las ecuaciones físicas lineales.
	;omega=-cs00*kk  ; CAMBIAR! para el modo de velocidad negativa (-) junto con la fase phase_an_v=!dpi
	phase_omega = omega*time  ; fase
	analitic, phase_omega,omega, time,  rho_an,vz_an,pres_an ;only TASK3, PROCEDURE ANALITIC, SOLUCION EXACTA " TIME ", devuelve  "*_an".  
        ;GUARDADO-------------
        itt_save=5000           ; Variable de guardado

        
	;---------------------------PLOT_1:VELOCITY--------------------------------------
	wset,0  ; selecciona la ventana 0
	
	;OPTIONS_TASK3-------------------------------
	plot,zz,(vz-v00)/cs00,$					  
             tit='Velocidad relativa',xtit='z',ytit='v/c!ds!n!3',$ 
             /xst,yr=[-0.1,10]*ampl                                    ; COMMENT:; ajuste de los ejes
	oplot,zz,vz_an/cs00,col=360,psym=3 

	;Maximo y su posicion
	vz_rel=vz/cs00                            
	max_vz_rel=max(vz_rel)                    
	arg_max_vz_rel=where(vz_rel eq max_vz_rel)

	oplot,[zz[arg_max_vz_rel]],[max_vz_rel],psym=4,thick=3
	;SPECIFICS
	plot_specifics, itt,time  
	;SAVE_FIG
	if itt eq itt_save then begin
		filename0='./IMAGES/'+'velocity'+'.png'  
		write_png, filename0,tvrd(/TRUE)         
	endif
	;------------------------------PLOT_2:DENSITY-----------------------------------
	wset,1
	
	;OPTIONS_TASK3----------------------------
	plot,zz,(rho-rho00)/rho00,xr=[z0,zf],$					
		tit='Densidad relativa',xtit='z',ytit='(!4q-q!d00!n)/q!d00!n!3',$ 
                /xst,yr=[-0.1,10]*ampl                                    ; COMMENT:; ajuste de los ejes
        oplot,zz,(rho_an-rho00)/rho00,col=360,psym=3				 

	;Maximo y su posicion
	rho_rel=(rho-rho00)/rho00 			
	max_rho_rel=max(rho_rel) 			
	arg_max_rho_rel=where(rho_rel eq max_rho_rel)   

	oplot,[zz[arg_max_rho_rel]],[max_rho_rel],psym=4,thick=3	
	;SPECIFICS
	plot_specifics,itt,time  
	;SAVE_FIG
	if itt eq itt_save then begin
	filename1='./IMAGES/'+'density'+'.png'
	write_png, filename1,tvrd(/TRUE)   
	endif
	;---------------------------PLOT_3:PRESSURE----------------------------------------
	wset,2
	;OPTIONS_TASK3-------
	plot,zz,(pres-p00)/p00,xr=[z0,zf],$
             tit='Presion relativa',xtit='z',ytit='(p-p!d00!n)/p!d00!n!3',$
             /xst,yr=[-0.1,10]*ampl                                    ; COMMENT:; ajuste de los ejes
        oplot,zz,(pres_an-p00)/p00,col=360,psym=3                      ;
        
	;Máximo y su posición
	pres_rel=(pres-p00)/p00           		
	max_pres_rel=max(pres_rel) 			
	arg_max_pres_rel=where( pres_rel eq max_pres_rel)

	oplot,[zz[arg_max_pres_rel]],[max_pres_rel],psym=4,thick=3 
	plot_specifics,itt,time 
	;SAVE_FIG
	if itt eq itt_save then begin
		filename2='./IMAGES/'+'pressure'+'.png'        
		write_png, filename2,tvrd(/TRUE)               
	endif 
	;----------------------------------------------------------------------------------
	;WAIT
	wait,0.001  

end
