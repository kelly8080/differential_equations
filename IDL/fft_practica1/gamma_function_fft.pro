pro gamma_function_fft, vz,rho,pres, itt,time  ;14/03
@grid_common
@init_common
@general_common
@fourier_common
	;-----------------------------------------------------------------------------------------------------------
	cs00=(gamm*p00/rho00)^(1d0/2d0) ; 15/03 velocidad inicial del sonido
	factor= ampl ;15/03 CUESTION DE PLOTS, ver ordenes de magnitud diferentes; PONER = 1 !!!!!
	;ITT_SAVE--------------------------------
	itt_save=12000        ;14/03 para guardar en la iteracion mencionada los graficos 
	
	;---------------------------PLOT_1:VELOCITY--------------------------------------
	wset,0  ; selecciona la ventana 0
	vz_rel=(vz-v00)/cs00 
	;OPTIONS_GAMMA_FUNCTION_FFT-------------------------------
	plot,zz,vz_rel,xr=[z0,zf],yr=[-1d0*max(vz_rel),+2d0*max(vz_rel)],$					  
		tit='Perturbación relativa de velocidad',xtit='z',ytit='(v!dz!n-v!d00!n)/c!ds!n!3'  ;SOLUCION NUMERICA ;  NEGRO!
	
	;Maximo y su posicion
	max_vz_rel=max(vz_rel)     &	arg_max_vz_rel=where(vz_rel eq max_vz_rel)

	oplot,[zz[arg_max_vz_rel]],[max_vz_rel],psym=4,thick=3	;MAXIMO DE LA SOLUCION NUMERICA ; ROMBO!
	;SPECIFICS
	plot_specifics, itt,time  
	;SAVE_FIG
	if itt eq itt_save then begin
		filename0='./IMAGES/'+'fft_velocity'+'.png'   
		write_png, filename0,tvrd(/TRUE)        ; Guarda en el filename el grafico. En GDL se necesita ImageMagick
	endif
	;------------------------------PLOT_2:DENSITY-----------------------------------
	wset,1
	rho_rel=(rho-rho00)/rho00 
	;OPTIONS_GAMMA_FUNCTION_FFT----------------------------
	plot,zz,rho_rel,xr=[z0,zf],yr=[-1d0*max(rho_rel),+2d0*max(rho_rel)],$
		tit='Perturbación relativa de densidad',xtit='z',ytit='(!4q-q!d00!n)/!4q!d00!n!3'  ;SOLUCION NUMERICA ; NEGRO!
		
	;Maximo y su posicion
	max_rho_rel=max(rho_rel) 	&	arg_max_rho_rel=where(rho_rel eq max_rho_rel)   

	oplot,[zz[arg_max_rho_rel]],[max_rho_rel],psym=4,thick=3	;MAXIMO DE LA SOLUCION NUMERICA, ROMBO!
	;SPECIFICS
	plot_specifics,itt,time  
	;SAVE_FIG
	if itt eq itt_save then begin
	filename1='./IMAGES/'+'fft_density'+'.png'    
	write_png, filename1,tvrd(/TRUE)              ; Guarda en el filename el grafico. En GDL se necesita ImageMagick
	endif
	;---------------------------PLOT_3:PRESSURE----------------------------------------
	wset,2
	pres_rel=(pres-p00)/p00 
	;OPTIONS_GAMMA_FUNCTION_FFT------------------
	plot,zz,pres_rel,xr=[z0,zf],yr=[-1d0*max(pres_rel),+2d0*max(pres_rel)],$
		tit='Presion relativa',xtit='z',ytit='(p-p!d00!n)/p!d00!n!3'  ;SOLUCION NUMERICA ;  NEGRO!

	;Maximo y su posicion
	max_pres_rel=max(pres_rel)   & 	arg_max_pres_rel=where( pres_rel eq max_pres_rel)

	oplot,[zz[arg_max_pres_rel]],[max_pres_rel],psym=4,thick=3 	;MAXIMO DE LA SOLUCION NUMERICA, ROMBO!
	;SPECIFICS
	plot_specifics,itt,time 
	;SAVE_FIG
	if itt eq itt_save then begin
		filename2='./IMAGES/'+'fft_pressure'+'.png'        
		write_png, filename2,tvrd(/TRUE)       ; Guarda en el filename el grafico. En GDL se necesita ImageMagick
	endif 
	
	

	;----------------ESPACIO_DE_FOURIER------------------------------------------------------------------------------------

	fast_fourier,vz         ;I!! FAST_FOURIER.pro : CALCULA LOS COEFS FOURIER 
	wset,3
	;--CONTROL
	control_mitad=round(npz/2d0)       
	control_elemento=10   ;freqs a ambos lados del 0; MAX: control_mitad-1 ;I!
	
	;--PLOTS
	plot,kk_n,cm,xr=[-control_elemento,control_elemento],$
		xtit='k!dn!n',ytit='c!dn!n',tit='Frecuencias de Fourier',/nodata ;Crea la ventana con titulos y color negro
	
	plots,kk_n[control_mitad-control_elemento:control_mitad+control_elemento],cm[control_mitad-control_elemento:control_mitad+control_elemento],$
			col=400,thick=1,psym=6

	
	for elemento=0,n_elements(kk_n)-1 do plots,[kk_n[elemento],kk_n[elemento]],[cm[elemento],0d0]  ; Barritas verticales
	
	plot_specifics,itt,time  

	;SAVE_FIG
	if itt eq itt_save then begin
		filename_fourier='./IMAGES/'+'fft_freqs'+'.png'
		write_png, filename_fourier,tvrd(/TRUE)
	endif
	

	;-----------------------------------------------------------------------------------------------------------------------------------------
	;WAIT
	wait,0.001  ;changes: 0.001>0.01
	
end
