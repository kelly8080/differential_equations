pro fast_fourier,vz       ;FAST FOURIER TRANSFORM.   ff is the function that we want to  get the Fourier Coefficients
;--
@grid_common
@init_common
@general_common
@fourier_common

fft_ff=FFT(vz[1:npz-2]) ; Celdas de guardia son la primera y la última. NPZ-2 componentes
fft_ff_rev=REVERSE(fft_ff)

factor_fourier= zf-z0    ;factor por el que hay que multiplicar los coefs. de fourier para que nos den los de la transformada,pero no es necesario
po_medio=ROUND((npz-2)/2d0)   ;to look for Position of Fourier Coefficients >0 , <0

cm_positive= ABS(fft_ff[ 0  :  po_medio  ] ) ;Modules for Fourier_coefficients (>=0),( = SQRT(REAL_PART(c_m)^(2d0)+IMAGINARY(c_m)^(2d0)))
cm_negative= ABS (fft_ff_rev [ 0 :  po_medio-2]) ;Modules for Fourier_coefficients (<0) , ""
cm=[REVERSE(cm_negative),cm_positive] ;07/04 add (*factor_fourier) when needed

kf=2*!pi/(zf-z0)  ; Main freq.

nn=dindgen(npz-2) + (- po_medio + 1) ;Vector que va de la componente -1023...0...1024 , por ejemplo
kk_n=nn*kf			 ;Numeros de onda de cada frecuencia				

end
;NOTA: Es muy importante que el vector, tanto fft_ff, cm y kk_n ,tengan
;npz-2 componentes, dado que son los valores que nosotros realmente
;tenemos por construccion, las celdas primera y última son las de
;guardia y corresponden a condiciones periódicas.
