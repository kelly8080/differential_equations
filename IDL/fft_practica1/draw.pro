PRO draw, rho,pres,vz,time,itt

@init_common
@grid_common  
@general_common
@fourier_common
  
device,decompose=0
!p.background=255
!p.color=0
;!p.charsize=1.1
loadct,39,/silent   

;----------------VARIABLES DE CONTROL---------------------------------------


itt_fourier=1                   ;iterador casual

;task (VARIABLE)   ;Valores :
;1a rutina ; 'P1_task3' , 'P1_task6'
;1� rutina_fft: 'gamma_function_fft'
;2a rutina : 'heat_conduction'


if switch_firsttime then begin            ; truco para hacer algo solo una vez
   device,window_state=wstat; COMMENT: para no destruir ventanas si ya est�n abiertas y ajustadas,"device,window_state=..."
   if not(wstat[0]) then window, xs=800,ys=300,xp=900,yp=0,tit='velocity (0)',  0  ;;VELOCITY 
   if not(wstat[1]) then window, xs=800,ys=300,xp=900,yp=310,tit='density (1)', 1  ;;DENSITY
   if not(wstat[2]) then window, xs=800,ys=300,xp=900,yp=750,tit='pressure (2)', 2  ;;PRESSURE      
   if not(wstat[3]) then window, xs=600,ys=300,xp=50,yp=750,tit='fourier_freqs(3)', 3 ;;FOURIER FRECUENCIES 
   switch_firsttime = 0 ;no repetir
endif

;TASKS_SELECTION-------

if task eq 'P1_task3' then P1_task3 ,vz,rho,pres, itt,time 	
if task eq 'P1_task6' then P1_task6 ,vz,rho,pres, itt,time
if task eq 'gamma_function_fft' then begin
   gamma_function_fft, vz,rho,pres,itt,time
   itt_fourier=0
endif
if task eq 'heat_conduction' then print,'hola'

end

;NOTAS Simbolos Latex: !3 es alfabeto alfanumerico. !4 alfabeto griego (ir probando) ;!u : upper_script , !d : down_script , !n : normal level

 
