pro PRUEBA ;16/03 Probar que la función h(z) es la serie de Fourier
@grid_common
@fourier_common

print,'La fft tiene N=',n_elements(fft_ff),'componentes'

max_freq=round(npz/2d0)
variable=0
fft_ff_rev=reverse(fft_ff)

zm=0d0

;Para 2048 puntos: 0..1024 son positivos, reverse(0..1023) son negativos 
for m=0,max_freq do begin
variable+=2*REAL_PART(fft_ff[m])*cos(2d0*!pi*m/(zf-z0)*(zz-zm)) ; Calcula las frecuencias positivas, COSENOS
variable+=2*IMAGINARY(fft_ff[m])*sin(2d0*!pi*m/(zf-z0)*(zz-zm)) ; Calcula las frecuencias positivas, SENOS
;for m=0,max_freq-2 do variable+=2*(fft_ff_rev[m])*cos(2d0*!pi*(-m-1d0)/(zf-z0)*(zz-zm));Calcula las frecuencias negativas

plot,zz,variable
xyouts,/norm,0.90,0.91,col=600,'freqs='+string(strtrim(max_freq,2));11/03 tiempo transcurrido

;plot,zz,variable2
end
