
pro dens2prim,rho,momz,energ,   vz,pres

; ----------------------------------------------------------------------
; ROUTINE dens2prim
;
;    PURPOSE:  Calculate the primitive variables vz and pres from the
;              mass, momentum and energy densities.
;
;    INPUT ARGUMENTS:   rho
;			momz
;			energ
;
;    COMMON BLOCKS: Note that some necessary input is passed via common
;                       blocks 
;
;    OUTPUT:   vz 
;	      pres
; ----------------------------------------------------------------------



@general_common.pro


;10/03 BIEN

vz = momz/rho
pres = (gamm-1d0)*(energ-momz*momz/2d0/rho)  







end
