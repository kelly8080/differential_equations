;PRACTICA 1
;TECNICAS DE SIMULACION NUMERICA
;AUTOR: Alvaro Rioboo de Larriva
;FECHA DE ENTREGA: 12/03/2018

;-----------------------------------------README----------------------------------------------------------------------------------------------------------
; Consejos generales sobre la interpretacion del codigo:
;  	Esta bien comentado , los comentarios en espanol son mios, ademas pone la fecha exacta en la que se hizo ese comentario o modificacion.
;	La fecha mas reciente indica el valor mas actual de la sentencia que acompana el comentario. 
;       Hay comentarios con lenguaje bastante simple o llano. Pido disculpas por la falta de rigor que acompane en ciertas ocasiones.
;	Se han intentado obviar en comentarios y plots , simbolos como  , tildes y demas, por ventajas obvias de codificacion y para evitar problemas de compatibilidades.	       
;
;Proposito de README:
;	Este fichero da cuenta de los cambios que hay que realizar al programa para poder realizar de forma más o menos sencilla las tareas que se piden en la practica.
;	Aunque en los programa ya queda bastante claro qué comandos pertenecen a cada tarea segun los comentarios, se agradece una pequeña recopilacion de los cambios.	
;CHECK-IN:
;Codigo: 
;	Ficheros .pro , .dat .  Detalle menor: se necesita una carpeta llamada "SAVES" en este directorio para que el "MAIN.pro" pueda guardar sus variables.También otra llamada "IMAGES"
; para que "draw.pro" pueda guardar los graficos
;
;
;Informe de la practica:		
;	
;	Deliverable2.pdf (PRACTICA 1: 2a parte)
;
;Cambios realizados :
;	README_fernando.txt (este fichero)   ( README.txt ya estaba cogido para mis autoexplicaciones)	
;
;----------------------------------------------------------------------------------------------------------------------------------------------------------------------

NOTA ENTREGABLE 2: IR DIRECTAMENTE A FFT_PRACTICA1 (segunda parte de la pr�ctica).

------------------------------PRACTICA 1:-----------------------------------------------------------------------------------------
TASK 1:
- No se necesitan cambios ; 10/03

TASK 2:
- No se necesitan cambios ; 10/03

TASK 3: 
- Asegurar que se ha elegido en "inputs.dat" , 'cosine' como variable "shape". Tambien algunos cambios de resoluci�n del intervalo como "1024L" para "nint"
- Cambios en "inputs.dat" de las condiciones de equilibrio , i.e: z0=4.1d0 , zf=+9.5d0, p00=2d0, rho00=1.2d0 , ampl=3e-4 (C) (cambiar primer elemento de las líneas correspondientes, en el caso de z0 y zf,
los dos primeros elementos).  ; 10/03
- Cambios en "draw.pro" de los comandos (plots) correspondientes a "TASK 3". Modificar el valor de la variable 'task' a 'task=3'

TASK 4:
- Cambios en "inputs.dat" del parametro de Courant , fcl (cambiar primer elemento de esta línea). Los valores concretos se contienen en el "Informe de la practica" ;10/03

TASK 5:
- Segundo modo sonico, hay que cambiar las condiciones iniciales en "initial.pro". Los valores concretos se encuentran en el "Informe de la practica" ; 10/03
  "initial.pro": add_phase_v=!dpi
- Si se quiere ver la solucion ANALITICA: Hay que cambiar:
 "analitic.pro" :  add_phase_v= !dpi
 "draw.pro" :  en el "if" de la tarea 3:    omega=-cs00*kk

O comentar las lineas correspondientes
TASK 6:
- Asegurar que se ha elegido en "inputs.dat", 'cosine' como variable shape. Asegurar numero de intervalos como "1024L" para "nint"
- Cambios en "inputs.dat" :   shape='gamma_function' (poner solo gamma_function al principio de la línea correspondiente a la variable "shape")      ; 10/03
- Cambios en "draw.pro" de los comandos (plots) correspondientes a "TASK6". Modificar el valor de la variable 'task' a task=6'

TASK 7:
- Cambios en "inputs.dat": v00 = cs00/4d0 (cambiar primer elemento de esta linea correspondiente a v00). ;10/03
Pero , � que es cs00 para el programa?, para ello tenemos que calcularlo con  p00 y rho00  antes, por eso la linea de v00 en "inputs.dat" se situara despues de estas definiciones; 10/03
Se podria tambien definir en el MAIN.pro, después de la definición de v00 por la lectura de "inputs.dat" y de rho00 y p00, pero asi lo haremos de forma mas metodica:
Cambio v00=( 5d0/3d0 * p00/rho00 )^(1/2d0) (poniendo el rhs de la anterior ecuacion al comienzo de la linea de v00 , y esta linea, después de las otras condiciones iniciales que se necesitan, pero esto ya se ha hecho directamente en el "inputs.dat"



------------------FFT_PRACTICA1:------------------------------------------------------------------------------------------------------------------------------

Para realizar las tareas:

"inputs.dat":

- Cambiar, la l�nea de la amplitud (ampl) donde corresponda en "inputs.dat" al valor concreto.
- Cambiar, si corresponde, las l�neas de itmax y timef   , correspondientes al numero maximo de iteraciones y al tiempo fisico maximo.
- Cambiar, el n�mero de intervalos para que las iteraciones corran m�s o menos deprisa y poder ver con comodidad como decaen los modos en el caso de amplitud media o grande. Tener en cuenta
que cuantos menos intervalos, mayor difusi�n num�rica tendremos.
- shape='gamma_function_fft' , pero �sto se define en "inputs.dat". 

Cambios en FFT_PRACTICA1:
- La tarea 'gamma_function_fft.pro' es llamada a partir de 'draw.pro' ,que lo �nico que hace ahora es distribuir tareas y crear las ventanas solamente una vez. En 'gamma_function_fft.pro' se hacen todos los plots correspondientes a las perturbaciones de densidad, presi�n y temperatura, pero ahora adem�s, saca un gr�fico nuevo con el diagrama de Fourier, mostrando las amplitudes 'cm' de los modos de Fourier en funci�n de las frecuencias espaciales 'kk_n'. Se ha a�adido el common 'fourier_common.pro', que contiene informaci�n sobre estas nuevas variables de la rutina fft (incorporada en IDL).
- El fichero 'PRUEBA.pro' da cuenta de la recomposici�n de la condici�n inicial h(z) tras la suma de sus arm�nicos, o de los que se quiera, y es una prueba directa de la veracidad de la ecuaci�n soluci�n en serie de Fourier de senos y cosenos presentada en el entregable.

----------------------------------------------------------------------------------------------------------------------------------------------------------------





