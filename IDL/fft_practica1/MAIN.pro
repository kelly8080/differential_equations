; pro MAIN  
@initial
@draw
@gamma_function_fft
@fast_fourier

; ------------------------------------------------------------------ ;
;   -> This is the CORE of the code                                  ;
;   -> It makes decisions and distributes tasks.                     ;
;   -> It also carries out a limited number of assignments itself.   ;
; ------------------------------------------------------------------ ;
; -------------;
; COMMON BLOCKS; 
; -------------;
; NOTE: common blocks allow different routines to share variables 
; (the corresponding RAM memory positions are shared among them) 
; (Note: "@" tells idl to insert here the lines contained in those files)

@grid_common
@init_common
@general_common
@fourier_common ; 16/03 definido por mi
; -------------
; initialize some of the variables in the common blocks

gamm=5d0/3d0  ; do not use the name 'gamma' for this variable(to avoid confusion with idl's Euler's Gamma function)
;11/03 Defino gamm en "inputs.dat", mejor

; -------------------------------;
; READ PARAMETERS FROM INPUT FILE;
; -------------------------------:

; OPEN THE INPUT DATA FILE
filename='./inputs.dat'
openr,unit,/get_lun,filename
strd=''   ; declare strd as a string variable for later use

; READ IN THE PARAMETERS FOR THE NUMERICAL MESH and CREATE THE MESH
readf,unit,nint  & npz=nint+2        
readf,unit,z0,zf  

; here you have to construct the grid of z points following the
; instructions in the script
dz=(zf-z0)/(npz-1)         
z_a= z0 - dz/2d0         &          z_b= zf + dz/2d0   ;puntos primero y �ltimo de la malla
zz=dindgen(npz)/(npz-1d0)*(z_b-z_a)+z_a  ;malla
undefine,z_a,z_b   ;borrar redundancia ( z_a=zz[0] y z_b=zz[npz-1]=zz[-1])

; READ IN MAX ITERATIONS, MAX TIME, OUTPUT PERIODICITY
readf, unit, itmax     
readf, unit, timef 
readf, unit, plotcad, storecad

readf, unit, strd ; just jump over an empty line in the input data file 

; READ IN THE PARAMETERS OF THE INITIAL CONDITION
readf, unit,form='(a25)',strd      &    inittype=strtrim(strd,2) 
readf, unit,form='(a25)',strd      &    shape=strtrim(strd,2) ;lee 25 primeros caracteres ; "inittype" es una variable de control que la definimos como "lo que haya cogido" quitando todos los espacios ("por delante y por detras" (2))   
  
readf, unit, strd ; jump over an empty line ;06/03 Leer inputs.dat para entender esto mejor!!!

; READ IN THE CFL PARAMETER
readf, unit, cfl

;09/03 MIOS
readf,unit,strd                 ; salta una linea

;READ IN THE EQUILIBRIUM VALUES
readf,unit,p00
readf,unit,rho00
readf,unit,v00
readf,unit,ampl  ;09/03 Deberia llamarse C , pero no se porque en los init_common.pro se llama asi, etc, no se si se refiere a otra cosa

readf,unit,strd

;READ TASK TO BE DONE
readf,unit,form='(a25)',strd    &    task=strtrim(strd,2)

;CLOSE inputs.dat 
free_lun,unit ; close and deallocate the unit

cs00=(gamm*p00/rho00)^(1d0/2d0); necesario definir aqui para TASK 7
;------------TASK7(Dejar descomentado solo una de las siguientes lineas)--------------
;11/03 TASK7: Descomentar una de estas lineas para la tarea 7. No se pueden leer variables en los "inputs.dat"
;v00=100d0*cs00 ;Este valor parece que deja congelada la onda anal�tica    
;v00=10d0*cs00
;v00=cs00
;v00=cs00/4d0
;v00=cs00/10d0
;v00=0        ;11/03 Recuperamos el caso de la tarea 3
;v00=-cs00/10d0
;v00=-cs00/4d0
;v00=-cs00      ; Este valor deja congelada la onda num�rica
;v00=-10d0*cs00
;v00=-100d0*cs00
;-------------------------------

; --------------------------------------
; READ PARAMETERS FROM INPUT FILE -- END
; --------------------------------------
switch_firsttime=1  ; this can serve later on as a switch to carry out certain 
                    ; operations the first time round and skip them later. 
itt_fourier=1       ;16/03 

; -------------------------
;  INITIAL CONDITIONS 
; -------------------------

initial                         ;06/03 ; this is a call to the routine initial (procedure)
                                ; that sets up the initial condition.
                                ; initial returns the arrays of the
                                ; primitive variables density, velocity and
                                ; pressure, i.e., rhoinit, vzinit, presinit
                                ;06/03 NO ES NECESARIO LLAMAR ARGUMENTOS

 momzinit = rhoinit*vzinit 
 energinit = presinit/(gamm-1d0) + rhoinit*vzinit*vzinit/2d0 
 csinit = (gamm*p00/rho00)^(1d0/2d0)   ; sound speed for the initial condition ; 06/03 A�adido un valor ;09/03 Hay que a�adir (gamm*press/rho)^(1/2)

; you may want to insert here a call to the initial plotting routine
; 10/03 'diagrams...'

;diagrams,rho00,pres00,v00,time,itt  ; 06/03 donde bla1 ,bla2 ,bla3,

;son variables, se llama asi a los "procedures" 09/03 mejor comentar
;la linea porque esos plots no aportan nada, ya con variables correctas
  
; -------------------------
;  INITIAL CONDITIONS - end   ;06/03 no hay que hacer nada mas
; -------------------------
 
; ------------------------------------------------------------
; DECLARE / INITIALIZE VARIABLES JUST BEFORE STARTING THE LOOP ;06/03 variables de "initial.pro"
; ------------------------------------------------------------

  vz = vzinit   &  rho    = rhoinit    &   pres = presinit   ; 10/03 initialize variables
  momz = momzinit &  energ  = energinit  &     cs = csinit   ; 10/03 ""
  rhon = rho      &  momzn  = momz       & energn = energ    ; 10/03 ""

itt=0L   
time=0d0 

; --------------------------------------------------------
; INITIALIZE VARIABLES JUST BEFORE STARTING THE LOOP - end
; --------------------------------------------------------

; ---------------
; BIG LOOP BEGINS
; ---------------
while itt lt itmax and time lt timef do begin   

;   CALCULATE FLUXES FROM DENSITIES ('fluxes' is the name of the subroutine) 
    fluxes,rho,momz, energ, mflz, momflzz, energflz

;   TIMESTEP 
    dt=cfl_new(rho,momz,cs)

;   UPDATE ACROSS TIMESTEP using the chosen numerical scheme 

    update, dt,rho,momz,energ, mflz,momflzz,energflz, rhon,momzn,energn

;   BOUNDARY CONDITIONS ('bcs' below is the name of the routine)
    bcs,'periodic','left',rhon     & bcs,'periodic','right',rhon  ;10/03 llama argumentos = type ,site , varr
    bcs,'periodic','left',momzn    & bcs,'periodic','right',momzn
    bcs,'periodic','left',energn   & bcs,'periodic','right',energn       

;   CALCULATE PRIMITIVE VARIABLES FROM THE DENSITIES    
    dens2prim, rhon,momzn,energn, vzn, presn  ;10/03
 
		
;   EXCHANGE NEW AND OLD VARIABLES
    rho = rhon & momz = momzn & energ = energn & pres = presn  & vz = vzn  ;10/03 inicializa para el proximo bucle los valores de la variable 

    itt++      ; this an idl/C/C++ alternative to saying:    itt = itt + 1   
    time += dt ; this an idl/C/C++ alternative to saying:  time = time + dt


;   STORE RESULTS - to store results in a file from time to time
    if itt mod storecad eq 0 then begin
      ;save, filename='./SAVES/variables_itt'+string(itt)+'.sav'
      ;save, /ROUTINES,filename='./SAVES/routines1.sav' 
      ;09/03 NOTA: Creo carpeta SAVES para salvar variables, rutinas.. etc
      ;09/03 NOTA: Compressed SAVE files can be restored by the RESTORE procedure in exactly the same manner as any other SAVE file. 

   endif

;   PLOT RESULTS 
    if itt mod plotcad eq 0 or itt eq 0 or time ge timef then begin
       draw, rho,pres,vz,time,itt ;09/03 ESTE ES EL BUEN PROGRAMA PARA DIBUJAR
       
                                ;El "diagrams.pro" es un programa
                                ;totalmente ignorable,solo da algunos
                                ;tips
       ;09/03 tv,dist(300) para ver el color 300 de la paleta de colores cargada
       ;09/03 xloadct para ver los colores; no disponible en GDL
    endif

endwhile

; --------------
; BIG LOOP - end
; --------------

print,'program finished.'+$
      '    itt= '+ strtrim(string(itt),2) +$
      '; time = '+ strtrim(string(time),2)

end

;NOTES: pro -- procedures, los argumentos se llaman con comas
;              siguiendo la palabra del procedure
;03/09 "undefine, a " por ejemplo, borra la variable a

