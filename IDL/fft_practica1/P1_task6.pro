pro P1_task6, vz,rho,pres, itt,time

	@grid_common
	@init_common
	@general_common

	;PLOTS 09/03 Aqui� ir�an los plots y wset para hacer cada uno,antes deberian
	;ir solo las ventanas (windows)
	;ITT_SAVE--------------------------------
	itt_save=100 ;11/03 para guardar en la iteracion mencionada los graficos 
	;---------------------------PLOT_1:VELOCITY--------------------------------------
	wset,0  ; selecciona la ventana 0
	;plot,zz,vz,xr=[z0,zf],xr=[z0,zf],xtit='z',ytit='velocity'   ; operaciones que hace (en la ventana seleccionada)

	;OPTIONS_TASK6-------------------------------
	plot,zz,vz/cs00,$					  ;TASK6
		tit='Velocidad relativa',xtit='z',ytit='v/c!ds!n!3'  ;TASK6 ;SOLUCION NUMERICA ;11/03   NEGRO!

	;Maximo y su posicion
	vz_rel=vz/cs00                            ;TASK6
	max_vz_rel=max(vz_rel)                    ;TASK6
	arg_max_vz_rel=where(vz_rel eq max_vz_rel);TASK6

	oplot,[zz[arg_max_vz_rel]],[max_vz_rel],psym=4,thick=3			;TASK6  ;MAXIMO DE LA SOLUCION NUMERICA ;11/03  ROMBO!
	;SPECIFICS
	plot_specifics, itt,time  ;11/03 Carga la rutina plot_specifics, es más cómodo agregar esta línea que cambiarla en todos los plots
	;SAVE_FIG
	if itt eq itt_save then begin
		filename0='./IMAGES/'+'velocity'+'_task'+strtrim(string(task),2)+'.png'   ;11/03 definimos un nombre del fichero (CREAR ANTES LA CARPETA)
		write_png, filename0,tvrd(/TRUE)               ;11/03 Guarda en el filename el grafico. En GDL se necesita ImageMagick
	endif
	;------------------------------PLOT_2:DENSITY-----------------------------------
	wset,1
	;plot,zz,rho,xr=[z0,zf],yr=[rho00-1*ampl,rho00+1*ampl],tit='Densidad',xtit='z',ytit='density'
	;OPTIONS_TASK6----------------------------
	plot,zz,(rho-rho00)/rho00,xr=[z0,zf],$					 ;TASK6
		tit='Densidad relativa',xtit='z',ytit='(!4q-q!d00!n)/q!d00!n!3' ;TASK6  ;SOLUCION NUMERICA ;11/03   NEGRO!

	;Máximo y su posición
	rho_rel=(rho-rho00)/rho00 			;TASK6
	max_rho_rel=max(rho_rel) 			;TASK6
	arg_max_rho_rel=where(rho_rel eq max_rho_rel)   ;TASK6

	oplot,[zz[arg_max_rho_rel]],[max_rho_rel],psym=4,thick=3		;TASK6 ;11/03 MAXIMO DE LA SOLUCION NUMERICA, ROMBO!
	;SPECIFICS
	plot_specifics,itt,time  ;11/03 Carga la rutina plot_specifics, es más comodo agregar esta linea que cambiarla en todos los plots
	;SAVE_FIG
	if itt eq itt_save then begin
		filename1='./IMAGES/'+'density'+'_task'+strtrim(string(task),2)+'.png'    ;11/03 definimos un nombre del fichero (CREAR ANTES LA CARPETA)
		write_png, filename1,tvrd(/TRUE)             ;11/03 Guarda en el filename el grafico. En GDL se necesita ImageMagick
	endif
	;---------------------------PLOT_3:PRESSURE----------------------------------------
	wset,2
	;plot,zz,pres,xr=[z0,zf],yr=[p00+100*ampl,p00+100*ampl],tit='Presion',xtit='z',ytit='pressure'
	;OPTIONS_TASK6------------------
	plot,zz,(pres-p00)/p00,xr=[z0,zf],$
		tit='Presion relativa',xtit='z',ytit='(p-p!d00!n)/p!d00!n!3'  ;TASK6  ;SOLUCION NUMERICA ;11/03   NEGRO!
	;Máximo y su posicion
	pres_rel=(pres-p00)/p00           		;TASK6
	max_pres_rel=max(pres_rel) 			;TASK6
	arg_max_pres_rel=where( pres_rel eq max_pres_rel);TASK6

	oplot,[zz[arg_max_pres_rel]],[max_pres_rel],psym=4,thick=3 		;TASK6 ;11/03 MAXIMO DE LA SOLUCION NUMERICA, ROMBO!
	;SPECIFICS
	plot_specifics,itt,time ;11/03 Carga la rutina plot_specifics, es mas comodo agregar esta linea que cambiarla en todos los plots
	;SAVE_FIG
	if itt eq itt_save then begin
		filename2='./IMAGES/'+'pressure'+'_task'+strtrim(string(task),2)+'.png'        ;11/03 definimos un nombre del fichero (CREAR ANTES LA CARPETA)
		write_png, filename2,tvrd(/TRUE)                    ;11/03 Guarda en el filename el grafico. En GDL se necesita ImageMagick
	endif 
	;----------------------------------------------------------------------------------
	;WAIT
	wait,0.001  ;09/03  changes: 0.001>0.01, mas rapido no va la iteracion 



end
