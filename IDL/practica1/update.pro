pro update, dt,  rho,momz,energ, mflz,momflzz,energflz, rhon,momzn,energn


; ----------------------------------------------------------------------
; ROUTINE update
;
;    PURPOSE:  Calculate variables at timestep n+1. This routines contains
;              the numerical scheme used in the code. 
;
;    INPUT ARGUMENTS:  - the densities at timestep n, namely rho, momz, energ
;                      - the fluxes mflz, momflzz, energflz
;
;    COMMON BLOCKS: Note that some necessary input is passed via common
;                       blocks (like the grid parameters and array zz, etc)
;
;    OUTPUT:  the densities at timestep n+1, namely rhon, momzn, energn
; ----------------------------------------------------------------------


@general_common.pro
@grid_common


; CARRY OUT HERE THE OPERATIONS NEEDED TO OBTAIN THE DENSITIES rhon, momzn
; and energn corresponding to the n+1 timestep

lambda =  dt / dz  ;10/03 (=DELTAt/DELTAx)   ;10/03  dz pasada por common, dt por nuestro procedure 


;-----------------------LAX_FRIEDRICH SCHEME------------------------------------------------------------------
;10/03  Dado que rhon, momz , energn se han inicializado antes, podemos simplemente asignarle los valores del

;10/03 Asignamos los valores de los arrays en los intervalos correctos para este método
rhon[1:-2] =  (rho[0:-3]+rho[2:-1]) /2d0   -   lambda/2d0 * (mflz[2:-1]-mflz[0:-3])       ; 10/03 
momzn[1:-2] = (momz[0:-3]+momz[2:-1])/2d0   -   lambda/2d0 * (momflzz[2:-1]-momflzz[0:-3])   ; 10/03 ""
energn[1:-2] =  (energ[0:-3]+energ[2:-1])/2d0 -  lambda/2d0 * (energflz[2:-1]-energflz[0:-3]) ; 10/03 ""

; IMPORTANT NOTE: no boundary conditions should be applied here!
;-----------------------------------------------------------------------------------------------------------





end

;10/03 NOTES: flujos mflz, momflz , energflz son las funciones que hay que usar después del lambda en el método de Lax_Friedrich. Se podría definir una función para esto, pero veo
;más representativo y directo hacerlo aquí. USAR FORMULA (A1) , del APPENDIX A: THE LAX_FRIEDRICH SCHEME
