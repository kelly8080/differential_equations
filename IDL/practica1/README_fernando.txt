;PRÁCTICA 1
;TÉCNICAS DE SIMULACIÓN NUMÉRICA
;AUTOR: Álvaro Riobóo de Larriva
;FECHA DE ENTREGA: 12/03/2018

;-----------------------------------------README----------------------------------------------------------------------------------------------------------
; Consejos generales sobre la interpretación del código:
;  	Está bien comentado , los comentarios en español son míos, además pone la fecha exacta en la que se hizo ese comentario o modificación.
;	La fecha más reciente indica el valor más actual de la sentencia que acompaña el comentario. 
;       Hay comentarios con lenguaje bastante simple o llano. Pido disculpas por la falta de rigor que acompañe en ciertas ocasiones.
;	Se han intentado obviar en comentarios y plots , simbolos como ñ , tildes y demás, por ventajas obvias de codificación y para evitar problemas de compatibilidades.	       
;
;Propósito de README:
;	Este fichero da cuenta de los cambios que hay que realizar al programa para poder realizar de forma más o menos sencilla las tareas que se piden en la práctica.
;	Aunque en los programa ya queda bastante claro qué comandos pertenecen a cada tarea según los comentarios, se agradece una pequeña recopilación de los cambios.	
;CHECK-IN:
;Código: 
;	Ficheros .pro , .dat .  Detalle menor: se necesita una carpeta llamada "SAVES" en este directorio para que el "MAIN.pro" pueda guardar sus variables.También otra llamada "IMAGES"
; para que "draw.pro" pueda guardar los gŕaficos
;Diagrama de flujo:
;	Deliverable1_flow_diagram.pdf
;
;Informe de la práctica:		
;	Deliverable1.pdf
;
;Cambios realizados :
;	README_fernando.txt (este fichero)   ( README.txt ya estaba cogido para mis autoexplicaciones)	
;
;----------------------------------------------------------------------------------------------------------------------------------------------------------------------


TASK 1:
- No se necesitan cambios ; 10/03

TASK 2:
- No se necesitan cambios ; 10/03

TASK 3: 
- Asegurar que se ha elegido en "inputs.dat" , 'cosine' como variable "shape". Tambien algunos cambios de resoluci�n del intervalo como "1024L" para "nint"
- Cambios en "inputs.dat" de las condiciones de equilibrio , i.e: z0=4.1d0 , zf=+9.5d0, p00=2d0, rho00=1.2d0 , ampl=3e-4 (C) (cambiar primer elemento de las líneas correspondientes, en el caso de z0 y zf,
los dos primeros elementos).  ; 10/03
- Cambios en "draw.pro" de los comandos (plots) correspondientes a "TASK 3". Modificar el valor de la variable 'task' a 'task=3'

TASK 4:
- Cambios en "inputs.dat" del parámetro de Courant , fcl (cambiar primer elemento de esta línea). Los valores concretos se contienen en el "Informe de la práctica" ;10/03

TASK 5:
- Segundo modo sónico, hay que cambiar las condiciones iniciales en "initial.pro". Los valores concretos se encuentran en el "Informe de la práctica" ; 10/03
  "initial.pro": add_phase_v=!dpi
- Si se quiere ver la solucion ANALÍTICA: Hay que cambiar:
 "analitic.pro" :  add_phase_v= !dpi
 "draw.pro" :  en el "if" de la tarea 3:    omega=-cs00*kk

O comentar las lineas correspondientes
TASK 6:
- Asegurar que se ha elegido en "inputs.dat", 'cosine' como variable shape. Asegurar numero de intervalos como "1024L" para "nint"
- Cambios en "inputs.dat" :   shape='gamma_function' (poner sólo gamma_function al principio de la línea correspondiente a la variable "shape")      ; 10/03
- Cambios en "draw.pro" de los comandos (plots) correspondientes a "TASK6". Modificar el valor de la variable 'task' a task=6'

TASK 7:
- Cambios en "inputs.dat": v00 = cs00/4d0 (cambiar primer elemento de esta línea correspondiente a v00). ;10/03
Pero , ¿ qué es cs00 para el programa?, para ello tenemos que calcularlo con  p00 y rho00  antes, por eso la linea de v00 en "inputs.dat" se situará después de estas definiciones; 10/03
Se podría también definir en el MAIN.pro, después de la definición de v00 por la lectura de "inputs.dat" y de rho00 y p00, pero así lo haremos de forma más metódica:
Cambio v00=( 5d0/3d0 * p00/rho00 )^(1/2d0) (poniendo el rhs de la anterior ecuación al comienzo de la línea de v00 , y ésta línea, después de las otras condiciones iniciales que se necesitan, pero esto ya se ha hecho directamente en el "inputs.dat"

