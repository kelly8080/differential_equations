;09/03 CREO EL PROGRAMA DRAW , copio y pego como modelo el diagrams
PRO draw, rho,pres,vz,time,itt
;09/03 anadidos zz,switch_firsttime; 09/03 esto parametros se añaden por "common blocks"
@init_common
@grid_common  ; No las agregamos a un bucle por ser costosas
@general_common 
;09/03 Especificaciones de los plots
device,decompose=0
!p.background=255
!p.color=0
;!p.charsize=1.1
loadct,39,/silent   
;----------------VARIABLE DE CONTROL---------------------------------------

task=6  ;11/03  solo dos posibles valores: task=3 o task=6. CAMBIAR!!!


if switch_firsttime then begin            ; truco para hacer algo solo una vez
   ;PLOT_1 velocity
   window, xs=800,ys=240,xp=900,yp=0,tit='velocity (0)',  0  ; abre una ventana de plot y la llama 1 (depende de la resolucion de pantalla)
   ;plot,zz,vz,/nodata,tit='Velocidad',xtit='z',ytit='velocity'

   ;PLOT_2 density
   window, xs=800,ys=240,xp=900,yp=310,tit='density (1)', 1  ;
   ;plot,zz,rho,/nodata,tit='Densidad',xtit='z',ytit='density'	

   ;PLOT_3 pressure
   window, xs=800,ys=240,xp=900,yp=750,tit='pressure (2)', 2  ;     
   ;plot,zz,pres,/nodata,tit='Presion',xtit='z',ytit='pressure'
   switch_firsttime = 0 ; 09/03 (=) no volver a repetir esta iteracion

endif
;CALCULO_NECESARIO---------------------
cs=(gamm*pres/rho)^(1d0/2d0)
cs00=(gamm*p00/rho00)^(1d0/2d0)

;INICIO_IF_TASKS--------------------------------
if task eq 3 then begin 
	;FUNCION_ANALITICA(para 'cosine')---------------------------------------------------
	lambda=zf-z0        ;11/03 Esto es porque hemos impuesto que sea periodica
	kk=2d0*!dpi/lambda ;11/03 Esto es obvio por la definicion de onda de toda la vida, y porque hemos pedido que sea periodica.

	omega= cs00*kk     ;11/03 Esto viene de la resolucion de las ecuaciones lineales.
	;omega=-cs00*kk		  ;11/03 CAMBIAR! para el modo de velocidad negativa (-) junto con la fase phase_an_v=!dpi

	phase_omega = omega*time  ;11/03 Es la fase que hay que anadir a la phase para dar cuenta del tiempo. 

	analitic, phase_omega,omega,time,rho_an,vz_an,pres_an;TASK3 ;11/03 No nos hace falta para el TASK6 la solucion analitica. Ademas, aun no sabemos si esa funcion es periodica .
	 
	;11/03 LLAMA AL PROCEDURE ANALITIC , QUE CALCULA LA SOLUCION EXACTA EN " TIME ", devuelve las variabes "*_an".  
	;-------------------------------------------------------------------
	;PLOTS 09/03 Aquí irían los plots y wset para hacer cada uno,antes deberian
	;ir solo las ventanas (windows)
	;ITT_SAVE--------------------------------
	itt_save=5000 ;11/03 para guardar en la iteracion mencionada los graficos 
	;---------------------------PLOT_1:VELOCITY--------------------------------------
	wset,0  ; selecciona la ventana 0
	;plot,zz,vz,xr=[z0,zf],xr=[z0,zf],xtit='z',ytit='velocity'   ; operaciones que hace (en la ventana seleccionada)

	;OPTIONS_TASK3-------------------------------
	plot,zz,vz/cs00,$					  ;TASK3
		tit='Velocidad relativa',xtit='z',ytit='v/c!ds!n!3'  ;TASK3  ;SOLUCION NUMERICA ;11/03   NEGRO!
	oplot,zz,vz_an/cs00,col=360,psym=3  		  	  ;TASK3 ;SOLUCION ANALITICA ;11/03   AZUL!

	;Maximo y su posicion
	vz_rel=vz/cs00                            ;TASK3
	max_vz_rel=max(vz_rel)                    ;TASK3
	arg_max_vz_rel=where(vz_rel eq max_vz_rel);TASK3

	oplot,[zz[arg_max_vz_rel]],[max_vz_rel],psym=4,thick=3			;TASK3  ;MAXIMO DE LA SOLUCION NUMERICA ;11/03  ROMBO!
	;SPECIFICS
	plot_specifics, itt,time  ;11/03 Carga la rutina plot_specifics, es mas comodo agregar esta linea que cambiarla en todos los plots
	;SAVE_FIG
	if itt eq itt_save then begin
		filename0='./IMAGES/'+'velocity'+'.png'   ;11/03 definimos un nombre del fichero (CREAR ANTES LA CARPETA)
		write_png, filename0,tvrd(/TRUE)               ;11/03 Guarda en el filename el grafico. En GDL se necesita ImageMagick
	endif
	;------------------------------PLOT_2:DENSITY-----------------------------------
	wset,1
	;plot,zz,rho,xr=[z0,zf],yr=[rho00-1*ampl,rho00+1*ampl],tit='Densidad',xtit='z',ytit='density'
	;OPTIONS_TASK3----------------------------
	plot,zz,(rho-rho00)/rho00,xr=[z0,zf],$					 ;TASK3
		tit='Densidad relativa',xtit='z',ytit='(!4q-q!d00!n)/q!d00!n!3' ;TASK3  ;SOLUCION NUMERICA ;11/03   NEGRO!
	oplot,zz,(rho_an-rho00)/rho00,col=360,psym=3				 ;TASK3  ;SOLUCION ANALITICA ;11/03  AZUL!

	;Maximo y su posicion
	rho_rel=(rho-rho00)/rho00 			;TASK3
	max_rho_rel=max(rho_rel) 			;TASK3
	arg_max_rho_rel=where(rho_rel eq max_rho_rel)   ;TASK3

	oplot,[zz[arg_max_rho_rel]],[max_rho_rel],psym=4,thick=3		;TASK3 ;11/03 MAXIMO DE LA SOLUCION NUMERICA, ROMBO!
	;SPECIFICS
	plot_specifics,itt,time  ;11/03 Carga la rutina plot_specifics, es más cómodo agregar esta línea que cambiarla en todos los plots
	;SAVE_FIG
	if itt eq itt_save then begin
	filename1='./IMAGES/'+'density'+'.png'    ;11/03 definimos un nombre del fichero (CREAR ANTES LA CARPETA)
	write_png, filename1,tvrd(/TRUE)             ;11/03 Guarda en el filename el grafico. En GDL se necesita ImageMagick
	endif
	;---------------------------PLOT_3:PRESSURE----------------------------------------
	wset,2
	;plot,zz,pres,xr=[z0,zf],yr=[p00+100*ampl,p00+100*ampl],tit='Presion',xtit='z',ytit='pressure'
	;OPTIONS_TASK3------------------
	plot,zz,(pres-p00)/p00,xr=[z0,zf],$
		tit='Presion relativa',xtit='z',ytit='(p-p!d00!n)/p!d00!n!3'   ;TASK3  ;SOLUCION NUMERICA ;11/03   NEGRO!
	oplot,zz,(pres_an-p00)/p00,col=360,psym=3 ; 			     ;TASK3  ;SOLUCION ANALITICA;11/03   AZUL!
	;Máximo y su posición
	pres_rel=(pres-p00)/p00           		;TASK3
	max_pres_rel=max(pres_rel) 			;TASK3
	arg_max_pres_rel=where( pres_rel eq max_pres_rel);TASK3

	oplot,[zz[arg_max_pres_rel]],[max_pres_rel],psym=4,thick=3 		;TASK3 ;11/03 MAXIMO DE LA SOLUCION NUMERICA, ROMBO!
	;SPECIFICS
	plot_specifics,itt,time ;11/03 Carga la rutina plot_specifics, es mas comodo agregar esta linea que cambiarla en todos los plots
	;SAVE_FIG
	if itt eq itt_save then begin
		filename2='./IMAGES/'+'pressure'+'.png'        ;11/03 definimos un nombre del fichero (CREAR ANTES LA CARPETA)
		write_png, filename2,tvrd(/TRUE)                    ;11/03 Guarda en el filename el grafico. En GDL se necesita ImageMagick
	endif 
	;----------------------------------------------------------------------------------
	;WAIT
	wait,0.001  ;09/03  changes: 0.001>0.01, mas rapido no va la iteracion 
endif


if task eq 6 then begin

	;PLOTS 09/03 Aqui� ir�an los plots y wset para hacer cada uno,antes deberian
	;ir solo las ventanas (windows)
	;ITT_SAVE--------------------------------
	itt_save=100 ;11/03 para guardar en la iteracion mencionada los graficos 
	;---------------------------PLOT_1:VELOCITY--------------------------------------
	wset,0  ; selecciona la ventana 0
	;plot,zz,vz,xr=[z0,zf],xr=[z0,zf],xtit='z',ytit='velocity'   ; operaciones que hace (en la ventana seleccionada)

	;OPTIONS_TASK6-------------------------------
	plot,zz,vz/cs00,$					  ;TASK6
		tit='Velocidad relativa',xtit='z',ytit='v/c!ds!n!3'  ;TASK6 ;SOLUCION NUMERICA ;11/03   NEGRO!

	;Maximo y su posicion
	vz_rel=vz/cs00                            ;TASK6
	max_vz_rel=max(vz_rel)                    ;TASK6
	arg_max_vz_rel=where(vz_rel eq max_vz_rel);TASK6

	oplot,[zz[arg_max_vz_rel]],[max_vz_rel],psym=4,thick=3			;TASK6  ;MAXIMO DE LA SOLUCION NUMERICA ;11/03  ROMBO!
	;SPECIFICS
	plot_specifics, itt,time  ;11/03 Carga la rutina plot_specifics, es más cómodo agregar esta línea que cambiarla en todos los plots
	;SAVE_FIG
	if itt eq itt_save then begin
		filename0='./IMAGES/'+'velocity'+'_task'+strtrim(string(task),2)+'.png'   ;11/03 definimos un nombre del fichero (CREAR ANTES LA CARPETA)
		write_png, filename0,tvrd(/TRUE)               ;11/03 Guarda en el filename el grafico. En GDL se necesita ImageMagick
	endif
	;------------------------------PLOT_2:DENSITY-----------------------------------
	wset,1
	;plot,zz,rho,xr=[z0,zf],yr=[rho00-1*ampl,rho00+1*ampl],tit='Densidad',xtit='z',ytit='density'
	;OPTIONS_TASK6----------------------------
	plot,zz,(rho-rho00)/rho00,xr=[z0,zf],$					 ;TASK6
		tit='Densidad relativa',xtit='z',ytit='(!4q-q!d00!n)/q!d00!n!3' ;TASK6  ;SOLUCION NUMERICA ;11/03   NEGRO!

	;Máximo y su posición
	rho_rel=(rho-rho00)/rho00 			;TASK6
	max_rho_rel=max(rho_rel) 			;TASK6
	arg_max_rho_rel=where(rho_rel eq max_rho_rel)   ;TASK6

	oplot,[zz[arg_max_rho_rel]],[max_rho_rel],psym=4,thick=3		;TASK6 ;11/03 MAXIMO DE LA SOLUCION NUMERICA, ROMBO!
	;SPECIFICS
	plot_specifics,itt,time  ;11/03 Carga la rutina plot_specifics, es más comodo agregar esta linea que cambiarla en todos los plots
	;SAVE_FIG
	if itt eq itt_save then begin
		filename1='./IMAGES/'+'density'+'_task'+strtrim(string(task),2)+'.png'    ;11/03 definimos un nombre del fichero (CREAR ANTES LA CARPETA)
		write_png, filename1,tvrd(/TRUE)             ;11/03 Guarda en el filename el grafico. En GDL se necesita ImageMagick
	endif
	;---------------------------PLOT_3:PRESSURE----------------------------------------
	wset,2
	;plot,zz,pres,xr=[z0,zf],yr=[p00+100*ampl,p00+100*ampl],tit='Presion',xtit='z',ytit='pressure'
	;OPTIONS_TASK6------------------
	plot,zz,(pres-p00)/p00,xr=[z0,zf],$
		tit='Presion relativa',xtit='z',ytit='(p-p!d00!n)/p!d00!n!3'  ;TASK6  ;SOLUCION NUMERICA ;11/03   NEGRO!
	;Máximo y su posicion
	pres_rel=(pres-p00)/p00           		;TASK6
	max_pres_rel=max(pres_rel) 			;TASK6
	arg_max_pres_rel=where( pres_rel eq max_pres_rel);TASK6

	oplot,[zz[arg_max_pres_rel]],[max_pres_rel],psym=4,thick=3 		;TASK6 ;11/03 MAXIMO DE LA SOLUCION NUMERICA, ROMBO!
	;SPECIFICS
	plot_specifics,itt,time ;11/03 Carga la rutina plot_specifics, es mas comodo agregar esta linea que cambiarla en todos los plots
	;SAVE_FIG
	if itt eq itt_save then begin
		filename2='./IMAGES/'+'pressure'+'_task'+strtrim(string(task),2)+'.png'        ;11/03 definimos un nombre del fichero (CREAR ANTES LA CARPETA)
		write_png, filename2,tvrd(/TRUE)                    ;11/03 Guarda en el filename el grafico. En GDL se necesita ImageMagick
	endif 
	;----------------------------------------------------------------------------------
	;WAIT
	wait,0.001  ;09/03  changes: 0.001>0.01, mas rapido no va la iteracion 






endif

end





;NOTES ;11/03 La posición del máximo se mueve con velocidad cs00 (TASK3). Para plots normales (por ejemplo, TASK4, TASK5) , dejar los plots de TASK3 puestos (más sencillo). Cambiar el valor de la variable "task" dependiendo de lo que se quiera al inicio del programa

;NOTAS ;11/03 Simbolos Latex: !3 es alfabeto alfanumerico. !4 alfabeto griego (ir probando) ;!u : upper_script , !d : down_script , !n : normal level

 
