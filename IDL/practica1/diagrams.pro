pro diagrams,rho,pres,vz,time,itt

@general_common.pro
@grid_common
@init_common

;08/03 PLOTEA LOS RESULTADOS POR PANTALLA


if switch_firsttime then begin
    print,'using a firsttime switch is a trick to do some operation '
    print,'only the first time the program enters this routine'
    


    switch_firsttime = 0
endif


;PLOTS
plot,[0,1],[0,0],yr=[0,1]
xyouts,/norm,0.3,0.6,'PLEASE ... '
xyouts,/norm,0.3,0.5,'BUILD A draw.pro ROUTINE'
xyouts,/norm,0.3,0.4,'THAT PLOTS THE SOLUTIONS YOU OBTAIN'
xyouts,/norm,0.8,0.8,'itt='+strtrim(string(itt),2)  ;09/03 definir itt 

;WAIT
wait,0.01 


end



