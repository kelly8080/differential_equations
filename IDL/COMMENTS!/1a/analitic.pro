pro ANALITIC,  phase_omega,omega,time,rho_an,vz_an,pres_an

@grid_common
@init_common
@general_common


; COMMENT: cs00 es mejor transmitirlo en el mismo common en que se transmiten
;          p00, rho00, etc. 

cs00=(gamm*p00/rho00)^(1d0/2d0)

if inittype eq 'sound wave' then begin

    if shape eq 'cosine' then begin
     ; *** YOU HAVE TO WRITE HERE THE PROPER DEFINITIONS ***
	;11/03 GENERAL PARAMETERS	
	lambda = zf-z0	    ;11/03 Longitud de onda.
	kk     = 2d0*!dpi/(zf-z0) ;11/03 Numero de onda.

	phase_an    =  2d0*!dpi*(zz-z0)/(zf-z0)+2d0*!dpi/5d0  -   phase_omega         ;11/03 Define aquí la fase  de la amplitud (depende con z en general)
	add_phase_v =  0d0                                                  ;11/03 Define aquí la fase adicional ( =0d0 MODO w=+cs*k) (= !pi MODO w= -cs*k) , "!dpi es pi en doble precision"
	phase_v_an  = phase_an  + add_phase_v                                     ;11/03 Define aquí el desfase entre la velocidad y las otras dos fases (iguales). 
	;09/03 function h(z) for 'cosine' 

	;11/03 FUNCION!!
	hh_an   = COS(phase_an) ;08/03 a definir en algun common ;FUNCION!!!
        hh_v_an = COS(phase_v_an)        
	
	;09/03 perturbation arrays
	rhoz00_an  = rho00*ampl*hh_an
	vz00_an    = cs00*ampl*hh_v_an
	presz00_an = gamm*p00*ampl*hh_an
	
	;09/03 initial conditions: ;( ECUACIONES (2.3) (las verdaderas cond. iniciales))
	rho_an  = rho00 +  rhoz00_an
        vz_an   = v00  +  vz00_an
        pres_an = p00  +  presz00_an

	;08/03 "hh" es la funcion que yo defino para cada uno de los 'shapes'
	;      NOTE: rhoinit, vzinit and presinit contain the *initial condition* 
	;            arrays, i.e., the superposition of the equilibrium constants
	;            rho00, pres00, vz00 and the perturbation arrays.
    endif

    if shape eq 'gamma_function' then begin ;11/03 NO TIENE MUCHO SENTIDO LA ANALÍTICA DADO QUE NO SABEMOS LA DEPENDENCIA TEMPORAL 
	;11/03	
	lambda = zf-z0	
	kk     = 2d0*!dpi/(zf-z0)

	;09/03 function h(z) for 'gamma_function' ; 09/03 TASK 6
	phase_an       =  6d0* ( zz-(z0+1.2d0*zf)/2.2d0 )    ;11/03 Define aquí la fase de la amplitud (depende con z en general)
	add_phase_v_an =  0d0                  ;11/03 Define aquí la fase adicional (=0d0 MODO w=+cs*k) (= !pi MODO w=-cs*k), "!dpi es pi en doble precision"
	phase_v_an     = phase_an + add_phase_v; 11/03 Define aquí el desfase entre la velocidad y las otras dos fases (iguales)FUNCION!!!
         	
	
	hh_an   =  GAMMA(2d0*(z-zc)/(zf-z0))/(10d0*COSH(phase)^2d0);FUNCION!!!
        hh_v_an =  GAMMA(2d0*(z-zc)/(zf-z0))/(10d0*COSH(phase)^2d0)
	
	;09/03 perturbation arrays :
	rhoz00_an  = rho00*ampl*hh_an
        vz00_an    = cs00*ampl*hh_v_an
        presz00_an = gamm*p00*ampl*hh_an
	
	;09/03 initial conditions:
	rho_an  = rho00 + rhoz00_an	
	vz_an   = v00  + vz00_an	
	pres_an = p00   + presz00_an
        	
    endif

endif
end
