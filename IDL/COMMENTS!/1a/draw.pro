

; COMMENT: dejar tantas lineas antiguas (y ya in�tiles) comentadas sin
;          borrarlas complica y emborrona el fichero un mont�n. 
;          Consejo: que los ficheros est�n tan 'limpios' y claros como 
;          sea posible, para llegar con facilidad a los sitios que uno tenga que cambiar. 

PRO draw, rho,pres,vz,time,itt

@init_common
@grid_common  ; No las agregamos a un bucle por ser costosas
@general_common 

device,decompose=0
!p.background=255
!p.color=0
;!p.charsize=1.1
loadct,39,/silent

;--VARIABLES_DE_CONTROL---------------------------------------

; COMMENT: esta elecci�n del valor de 'task' es un par�metro global del c�lculo. Deber�a hacerse
;          en el fichero inputs.dat y transmitirse a draw via common. 
task=3  ;solo dos posibles valores: task=3 o task=6. CAMBIAR!!!

if switch_firsttime then begin            ; truco para hacer algo solo una vez
       device,window_state=wstat ; COMMENT:;  para no destruir ventanas si ya est�n abiertas y ajustadas, usar;  device,window_state=..., como sigue:
    if not(wstat[0]) then window, xs=800,ys=300,xp=900,yp=0,tit='velocity (0)',  0  ;;VELOCITY 
    if not(wstat[1]) then window, xs=800,ys=300,xp=900,yp=310,tit='density (1)', 1  ;;DENSITY
    if not(wstat[2]) then window, xs=800,ys=300,xp=900,yp=750,tit='pressure (2)', 2  ;;PRESSURE     
    switch_firsttime = 0 ; no repetir 
endif



;CALCULO_NECESARIO---------------------
cs=(gamm*pres/rho)^(1d0/2d0)
cs00=(gamm*p00/rho00)^(1d0/2d0)

;-IF_TASKS--------------------------------
if task eq 3 then begin 
	;FUNCION_ANALITICA(para 'cosine')---------------------------------------------------
	lambda=zf-z0        ;periodica
	kk=2d0*!dpi/lambda ;periodica

	omega= cs00*kk     ;soluci�n f�sica
	;omega=-cs00*kk    ;modo de velocidad negativa (-) junto con la fase phase_an_v=!dpi

	phase_omega = omega*time  ;fase 

	analitic, phase_omega,omega,time,rho_an,vz_an,pres_an ;calcula solucion exacta en " TIME ", variables "*_an".  
	
	;ITT_SAVE--------------------------------
	itt_save=5000 ;guardar en iteracion


        ;---------------------------PLOT_1:VELOCITY--------------------------------------
	wset,0
	
	plot,zz,(vz-v00)/cs00,$					  
		tit='Perturbaci�n relativa de velocidad',xtit='z',ytit='(v-v!d00)!n/c!ds!n!3',$  
                                 /xst, yr=[-1,1]*ampl*1.3,/yst,xmar=[12,5]  ;SOLUCION NUMERICA ;  NEGRO!

	oplot,zz,vz_an/cs00,col=360,psym=3  		  	  

        oplot,zz,0*zz+ampl,line=2
        oplot,zz,0*zz-ampl,line=2
	;Maximo y su posicion
	vz_rel=vz/cs00     &	max_vz_rel=max(vz_rel)   &	arg_max_vz_rel=where(vz_rel eq max_vz_rel)

	oplot,[zz[arg_max_vz_rel]],[max_vz_rel],psym=4,thick=3			;TASK3  ;MAXIMO DE LA SOLUCION NUMERICA ;11/03  ROMBO!
	
	plot_specifics, itt,time  ;tunning

        ;SAVE_FIG
	if itt eq itt_save then begin
		filename0='./IMAGES/'+'velocity'+'.png'   ;11/03 definimos un nombre del fichero (CREAR ANTES LA CARPETA)
		write_png, filename0,tvrd(/TRUE)               ;11/03 Guarda en el filename el grafico. En GDL se necesita ImageMagick
	endif

        ;------------------------------PLOT_2:DENSITY-----------------------------------
        wset,1
	plot,zz,(rho-rho00)/rho00,xr=[z0,zf],$					
		tit='Perturbaci�n relativa de densidad',xtit='z',ytit='(!4q-q!d00!n)/q!d00!n!3',$ 
                           /xst, yr=[-1,1]*ampl*1.3,/yst,xmar=[12,5]  ;SOLUCION NUMERICA ; NEGRO!

        oplot,zz,(rho_an-rho00)/rho00,col=360,psym=3 ;SOLUCION ANALITICA ;AZUL

        ; COMMENT: 
            ;    se aconseja poner lineas horizontales en los tres dibujos a
                                ;    la altura de los m�ximos y m�nimos
                                ;    esperados te�ricamente para la onda.
        oplot,zz,0*zz+ampl,line=2
        oplot,zz,0*zz-ampl,line=2

	; COMMENT: esto se hace as�, en una l�nea:
        max_rho_rel=max((rho-rho00)/rho00, arg_max_rho_rel)


	oplot,[zz[arg_max_rho_rel]],[max_rho_rel],psym=4,thick=3		;MAXIMO DE LA SOLUCION NUMERICA, ROMBO!

                                ; COMMENT: este rombo que se ha programado va por definici�n
                                ; encima del m�ximo de la soluci�n
                                ; num�rica. Lo que se ped�a era calcular la
                                ; posici�n de un punto que empiece
                                ; coincidiendo con el m�ximo de la funci�n
                                ; inicial y que lo forcemos a moverse a
                                ; velocidad del sonido del equilibrio (por tanto: dando
                                ; saltos espaciales de un paso temporal al
                                ; siguiente de tama�o cs00*dt)  (adem�s, al
                                ; dibujarlo, coloc�ndolo en cada
                                ; instante a la altura del m�ximo)

	;SPECIFICS
	plot_specifics,itt,time  ;tunning
	;SAVE_FIG
	if itt eq itt_save then begin
	filename1='./IMAGES/'+'density'+'.png'    
	write_png, filename1,tvrd(/TRUE)          
        endif
        
	;---------------------------PLOT_3:PRESSURE----------------------------------------
	wset,2
	
                                ; COMMENT: lo que se dibuja es
                                ; tit='Perturbaci�n relativa de presi�n' (y
                                ; an�logo para los otros dibujos)
	plot,zz,(pres-p00)/p00,xr=[z0,zf],$
		  tit='Perturbaci�n relativa de presi�n',ytit='(p-p!d00!n)/p!d00!n!3',$   ;TASK3  ;SOLUCION NUMERICA ;11/03   NEGRO!$
                                   /xst, yr=[-1,1]*ampl*1.3*gamm,/yst,xmar=[12,5]

        oplot,zz,(pres_an-p00)/p00,col=360,psym=3 ;SOLUCION ANALITICA;AZUL!
        
        oplot,zz,0*zz+ampl,line=2 ;lineas horizontales
        oplot,zz,0*zz-ampl,line=2

        ;Maximo y su posicion
	pres_rel=(pres-p00)/p00 &	max_pres_rel=max(pres_rel) &	arg_max_pres_rel=where( pres_rel eq max_pres_rel)

	oplot,[zz[arg_max_pres_rel]],[max_pres_rel],psym=4,thick=3 ; MAXIMO SOLUCION NUMERICA, ROMBO!
	
	plot_specifics,itt,time ;tunning

        ;SAVE_FIG
	if itt eq itt_save then begin
		filename2='./IMAGES/'+'pressure'+'.png'       
		write_png, filename2,tvrd(/TRUE)              
	endif 
	;----------------------------------------------------------------------------------
	
	wait,0.001  
endif


; COMMENT: hacer una parte completamente separada de directivas de dibujo etc
;          para cada tarea es muy pesado. Los cambios que quieras hacer que afecten a
;          las dos tareas, los tienes que implementar dos veces .... 


if task eq 6 then begin

	;ITT_SAVE--------------------------------
	itt_save=100
	;---------------------------PLOT_1:VELOCITY--------------------------------------
	wset,0 
	
	plot,zz,vz/cs00,$					  
		tit='Perturbaci�n relativa de velocidad',xtit='z',ytit='(v-v!d00!n)/c!ds!n!3'  ;SOLUCION NUMERICA; NEGRO!

	;Maximo y su posicion
	vz_rel=vz/cs00     &  	max_vz_rel=max(vz_rel)   &	arg_max_vz_rel=where(vz_rel eq max_vz_rel)

	oplot,[zz[arg_max_vz_rel]],[max_vz_rel],psym=4,thick=3			;TASK6  ;MAXIMO DE LA SOLUCION NUMERICA ;11/03  ROMBO!
	
	plot_specifics, itt,time ;tunning
	;SAVE_FIG
	if itt eq itt_save then begin
		filename0='./IMAGES/'+'velocity'+'_task'+strtrim(string(task),2)+'.png'   
		write_png, filename0,tvrd(/TRUE) 
             endif
        
	;------------------------------PLOT_2:DENSITY-----------------------------------
	wset,1
	
	plot,zz,(rho-rho00)/rho00,xr=[z0,zf],$					
		tit='Perturbaci�n relativa de densidad',xtit='z',ytit='(!4q-q!d00!n)/q!d00!n!3'  ;SOLUCION NUMERICA ;  NEGRO!

	;Máximo y su posición
	rho_rel=(rho-rho00)/rho00  &   max_rho_rel=max(rho_rel)   &   arg_max_rho_rel=where(rho_rel eq max_rho_rel)   

        oplot,[zz[arg_max_rho_rel]],[max_rho_rel],psym=4,thick=3  ;MAXIMO DE LA SOLUCION NUMERICA, ROMBO!
	
	plot_specifics,itt,time  ;SPECIFICS

        ;SAVE_FIG
	if itt eq itt_save then begin
		filename1='./IMAGES/'+'density'+'_task'+strtrim(string(task),2)+'.png'    
		write_png, filename1,tvrd(/TRUE)             
	endif
	;---------------------------PLOT_3:PRESSURE----------------------------------------
	wset,2

	plot,zz,(pres-p00)/p00,xr=[z0,zf],$
             tit='Perturbaci�n relativa de presi�n',xtit='z',ytit='(p-p!d00!n)/p!d00!n!3',$
             /xst, yr=[-1,1]*ampl*1.3*gamm,/yst,xmar=[12,5]
        
	;Maximo y su posicion
	pres_rel=(pres-p00)/p00     &   max_pres_rel=max(pres_rel) &	arg_max_pres_rel=where( pres_rel eq max_pres_rel)

	oplot,[zz[arg_max_pres_rel]],[max_pres_rel],psym=4,thick=3 ;MAXIMO DE LA SOLUCION NUMERICA, ROMBO!
	
	plot_specifics,itt,time ;SPECIFICS
	;SAVE_FIG
	if itt eq itt_save then begin
		filename2='./IMAGES/'+'pressure'+'_task'+strtrim(string(task),2)+'.png'        
		write_png, filename2,tvrd(/TRUE)                    
	endif 
	;----------------------------------------------------------------------------------
	;WAIT
	wait,0.001 
endif

end

;NOTAS ;11/03 Simbolos Latex: !3 es alfabeto alfanumerico. !4 alfabeto griego (ir probando) ;!u : upper_script , !d : down_script , !n : normal level

 
