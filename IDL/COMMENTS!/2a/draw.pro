PRO draw, rho,pres,vz,time,itt
;09/03 anadidos zz,switch_firsttime; 09/03 esto parametros se anaden por "common blocks"

@init_common
@grid_common  ; No las agregamos a un bucle por ser costosas
@general_common
@fourier_common
;09/03 Especificaciones de los plots
device,decompose=0
!p.background=255
!p.color=0
;!p.charsize=1.1
loadct,39,/silent   
;----------------VARIABLES DE CONTROL---------------------------------------

; COMMENT: esta elecci�n del valor de 'task' es un par�metro global del c�lculo. Deber�a hacerse
;          en el fichero inputs.dat y transmitirse a draw via common. 
;variable: task
itt_fourier=1  ; 16/03 iterador para hacer una vez algo

if switch_firsttime then begin            ; truco para hacer algo solo una vez
    device,window_state=wstat  ; COMMENT; para no destruir ventanas si ya est�n abiertas y ajustadas, usar"device,window_state":
    if not(wstat[0]) then window, xs=800,ys=300,xp=900,yp=0,tit='velocity (0)',  0 ;;VELOCITY 
    if not(wstat[1]) then window, xs=800,ys=300,xp=900,yp=310,tit='density (1)', 1 ;;DENSITY
    if not(wstat[2]) then window, xs=800,ys=300,xp=900,yp=750,tit='pressure (2)', 2  ;;PRESSURE     
    if not(wstat[3]) then window, xs=600,ys=300,xp=50,yp=750,tit='fourier_freqs(3)', 3 ;;FOURIER_FREQS		   
    switch_firsttime = 0 ;no repetir
endif

;IF_TASKS--------------------------------
if task eq 'P1_task3' then begin P1_task3 ,vz,rho,pres, itt,time	

if task eq 'P1_task6' then begin P1_task6 ,vz,rho,pres, itt,time
if task eq 'gamma_function_fft' then begin
	gamma_function_fft, vz,rho,pres,itt,time
	itt_fourier=0
endif
end


;NOTES ;11/03 La posicion del maximo se mueve con velocidad cs00 (TASK3). Para plots normales (por ejemplo, TASK4, TASK5) , dejar los plots de TASK3 puestos (más sencillo). Cambiar el valor de la variable "task" dependiendo de lo que se quiera al inicio del programa

;NOTAS ;11/03 Simbolos Latex: !3 es alfabeto alfanumerico. !4 alfabeto griego (ir probando) ;!u : upper_script , !d : down_script , !n : normal level

 
