PRO INITIAL

; ----------------------------------------------------------------------
; ROUTINE INITIAL
;
;    PURPOSE:  Calculate the arrays of density, velocity and pressure 
;                 at time t=0. 
;    INPUT ARGUMENTS: they are passed via common block init_common
;              inittype:   char variable with the choice of initial condition
;                 shape:   char variable with some subsidiary choice
;                 
;	** please fill in the list with your choices **
;            
;    COMMON BLOCKS: Note that some additional necessary input is passed via
;                   other common blocks (like the grid parameters,  etc)
;
;    OUTPUT:  the arrays rhoinit, vzinit, presinit (common block)
; ----------------------------------------------------------------------
@general_common
@grid_common
@init_common

cs00=(gamm*p00/rho00)^(1d0/2d0)
;--------------------------------------------------------
if inittype eq 'sound wave' then begin
    if shape eq 'cosine' then begin
     ; *** YOU HAVE TO WRITE HERE THE PROPER DEFINITIONS ***
	; GENERAL PARAMETERS	
	lambda=zf-z0	
	kk=2d0*!dpi/(zf-z0)
	
        ; FASES DE LA ONDA
	phase=2d0*!dpi*(zz-z0)/(zf-z0)+2d0*!dpi/5d0 ;amplitud (depende con z en general)
	add_phase_v= 0d0                  ; ( =0d0 MODO w=+cs*k) (= !pi MODO w= -cs*k) , "!dpi es pi en doble precision"
	phase_v= phase  +  add_phase_v     ;fase para v   
	
	;FUNCTION!!
        hh=COS(phase)
        hh_v=COS(phase_v)        
	
	;09/03 perturbation arrays
	rho_pert=rho00*ampl*hh
	v_pert=cs00*ampl*hh_v
	pres_pert=gamm*p00*ampl*hh
	
	;09/03 initial conditions: ;( ECUACIONES (2.3) (las verdaderas cond. iniciales))
	rhoinit = rho00 +  rho_pert
        vzinit  =  v00  +  v_pert
        presinit = p00  +  pres_pert
    endif

    if shape eq 'gamma_function' then begin

	;function h(z) for 'gamma_function' 
	phase  =  6d0* ( zz-(z0+1.2d0*zf)/2.2d0 )^2d0   ;amplitud (depende con z en general)
	add_phase_v = 0d0             ; ( =0d0 MODO w=+cs*k) (= !pi MODO w= -cs*k) , "!dpi es pi en doble precision"
	phase_v=phase + add_phase_v ;fase para v
	
		
        ;FUNCTION!!
        zc=2d0*z0-1.3d0*zf
	hh=GAMMA(2d0*(zz-zc)/(zf-z0))/(10*COSH(phase)^2);FUNCTION!!!
        	
	;perturbation arrays :
	rho_pert=rho00*ampl*hh
        v_pert=cs00*ampl*hh_v
        pres_pert=gamm*p00*ampl*hh
	
	;initial conditions:
	presinit  = p00   + pres_pert
	rhoinit = rho00 + rho_pert
        vzinit  = v00  + v_pert 
	
    endif

    if shape eq 'gamma_function_fft' then begin
	
	arg  =  6d0* ( zz-(z0+1.2d0*zf)/2.2d0 )^2d0   ;amplitud (depende con z en general)
		
	;function hh
	zc=2d0*z0-1.3d0*zf
	hh=GAMMA(2d0*(zz-zc)/(zf-z0))/(10d0*COSH(arg)^2d0)

        ;GAUSSIAN_FUNCTION gg_2ªparte(z)------------------------------------------------------------
	zm= 6d0 ; 14/03 Valor centrado en el pico de la exponencial. Típicamente tendrá la forma:  algun_z + z0	
	WW=2d0/(zf-z0)  ;semianchura FWHM (en principio) de la gaussiana (desviacion estandar)
	
	;14/03;FUNCTION_GAUSSIAN
	C0=10d0
	gg_primera=C0*exp(-(zz-zm)^2d0/ WW^2d0)         ;gaussiana "ansatz"
							
	int_hh=total(hh[1:-2])*dz          ;'total' =sumatorio
	int_gg_primera=total(gg_primera[1:-2])*dz

	;CONDICION:  int(hh)+CTE_EXP*int(gg)=0
	CTE_EXP= - (int_hh/int_gg_primera)
	
	;Redefino la gaussiana 
	gg=CTE_EXP*gg_primera         ; exp(-(zz-zm)^2d0/ WW^2d0)
	int_gg=total(gg[1:-2])*dz	
	
	;f(z)[ff]=h1_(z)[hh] + g(z)[gg] :
	ff=hh+gg

	;perturbation arrays :
	rho_pert=rho00*ampl*ff
        v_pert=cs00*ampl*ff
        pres_pert=gamm*p00*ampl*ff
	
	;initial conditions:
	presinit  = p00   + pres_pert
	rhoinit = rho00 + rho_pert
        vzinit  = v00  + v_pert 

	;---MESSAGE-----
	print,'routine gamma_function_fft: Working on it , please, notice every error the routine has.'
    	print,'int_gg=',int_gg,'	int_hh=',int_hh
	print,'CTE_EXP=',CTE_EXP    
	print,'zm=',zm
	print,'W=',WW
	endif
	
   if inittype eq 'other' then begin
        print,'routine initial: no other condition is implemented yet'
        stop
   endif

endif 

end
