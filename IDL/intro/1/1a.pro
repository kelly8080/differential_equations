;pro 1a
@grid_common      ;Queremos que las rutinas compartan nuestras variables
@function_common
@general_common
;----------------------------------------------
potencia=6d0       ;POTENCIA DEL N DE INTERVALOS  :: probar 4,5,6,7,8,9,10
nint=2L^(potencia) ; N DE INTERVALOS
npx=nint+1         ; N DE PUNTOS (nint+1)
;---------------------------------------------

x0=-1.5d0  ; VALOR INICIAL 
xf=6d0   ; VALOR FINAL

xx=sampling(xf,x0,npx) ;SAMPLEADO DE PUNTOS (MALLA)
DELTAx=xx[1]-xx[0]  ;INTERVALO UNIFORME
;----------------FUNCION_ANALITICA (en los puntos i)-------------------------------------------
hh_an=2d0 * (1d0 + TANH(xx)) * SIN(xx) * COSH((xx+2d0)/2d0)^(-1d0)  ;FUNCION ANALITICA en los puntos i

hp_an=COSH((2d0+xx)/2d0) *(2d0*COSH(xx)^(-2d0)*SIN(xx)  +  (1d0+TANH(xx))*(2d0*COSH(xx)-SIN(xx)*TANH((2d0+xx)/2d0)) ) ;FUNCION ANALITICA en los puntos i
;FUNCION_ANALITICA (en los puntos i+1/2)
xx_12= sampling(xf,x0,npx-1d0) + DELTAx/2d0    &   xx_12=xx_12[0:-2]  ;baja uno la long. del vector

hp_an_12=COSH((2d0+xx_12)/2d0) *(2d0*COSH(xx_12)^(-2d0)*SIN(xx_12)  +  (1d0+TANH(xx_12))*(2d0*COSH(xx_12)-SIN(xx_12)*TANH((2d0+xx_12)/2d0))) ;FUNCION ANALITICA (en los puntos i+1/2)
;--------------FUNCION_NUMERICA-----------------------------------------------------------
hp_num= (hh_an[1:-1]-hh_an[0:-2])/(xx[1:-1]-xx[0:-2])
; Recordar que hp_an(i) aprox. hp_num(i+1/2)


;----------------plots-----------
task='1a'    ;'1a','1c'

plots_1


;-------------------------------------------------------





end
