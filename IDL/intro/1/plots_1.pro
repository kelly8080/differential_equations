pro plots_1            ;PROCEDURE, solo toma la tarea task para hacer cosas
@grid_common       ;Aqui hay que ponerlo
@function_common    ;Aqui hay que ponerlo 
@general_common
;-------------------------
device,decompose=0
!p.background=255
!p.color=0
;!p.charsize=1.1
loadct,39,/silent  
;-------------------------------------


if task eq '1a' then begin
	;---------------------------
	window,0
	wset,0
	xr=minmax(xx) & factor_plot=3
	plot,xx,hh_an,xstyle=1;,xr=[-factor_plot*xr[0],factor_plot*xr[1]]
	oplot,xx,hp_an,col=500,psym=1
	;---------------------
	window,1
	wset,1
	plot,xx,hp_an_12,line=0
	oplot,xx,hp_num,col=300,psym=2

	;--------------------------
	
	window,2
	wset,2
	plot,xr=[1,1024],yr=[0,1]
	itt_min=4   &  itt_max=9
	for itt=itt_min,itt_max do begin
		potencia=itt
		nint=2L^(itt)
		convergence
		oplot,[alog(nint)],[alog(error_max)],psym=2,thick=3
	endfor

	;-------------------------------
endif


if task eq '1c' then begin
	window,0


endif



end
