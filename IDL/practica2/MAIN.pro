; pro MAIN
;---COMPILE_ROUTNINES
@initial
@fluxes
@cfl_new
@update
@bcs
@draw
;--me
@analytical_gaussian
@gaussian
@parabola
@blanket
@high_opacity
;--COMMON_BLOCKS
@grid_common
@init_common
@general_common


gamm=5d0/3d0  ; do not use the name 'gamma' for this variable(to avoid confusion with idl's Euler's Gamma function)

; -------------------------------;
; READ PARAMETERS FROM INPUT FILE;
; -------------------------------:

; OPEN THE INPUT DATA FILE
filename='./inputs.dat'
openr,unit,/get_lun,filename
strd=''   ; declare strd as a string variable for later use

; READ IN THE PARAMETERS FOR THE NUMERICAL MESH and CREATE THE MESH
readf,unit,nint    &      npz = nint+2        ; nint cells (+2 guard cells) ->  npz points
readf,unit,za,zb                              ;physical limits of the domain  

;GRID -> T,kappa
dz = (zb-za)/(npz-1)                          ;long. intervalo
z_0= za-dz/2d0           &   z_f= zb+dz/2d0   ; extremos de la malla
zz = dindgen(npz)/(npz-1d0)*(z_f-z_0)+z_0        ;malla bien definida 
undefine, z_0,z_f                             ;evitar redundancias: (z_0=zz[0], z_f=zz[npz-1])

zz_flux=dindgen(npz-1)/(npz-2d0)*(zb-za)+za ;FLUJO! npz-1 componentes (limites celdas)

; READ IN MAX ITERATIONS, MAX TIME, OUTPUT PERIODICITY
readf, unit, itmax ,itt_save  ; iteracion maxima, iteracion de guardado de graficos  
readf, unit, timef 
readf, unit, plotcad, storecad

readf, unit, strd ; just jump over an empty line in the input data file 

; READ IN THE PARAMETERS OF THE INITIAL CONDITION & B.C.'s
readf,unit,form='(a25)',strd      &    inittype=strtrim(strd,2)

readf, unit, strd

readf,unit,form='(a25)',strd      &    shape=strtrim(strd,2)     ;shape : elige la forma funcional de la condicion inicial T(z)
; READ IN THE PARAMETERS OF BOUNDARY CONDITIONS
readf,unit,form='(a25)',strd      &    type_left=strtrim(strd,2) ;(A) type_left : B.C. punto IZQUIERDA

readf, unit, strd

readf,unit,form='(a25)',strd      &    type_right=strtrim(strd,2) ;(B) type_right : B.C. punto DERECHA

readf, unit, strd 

; READ IN THE CFL PARAMETER
readf, unit, cfl

readf,unit,strd   
;READ IN SOME OTHER PARAMETERS
readf, unit, kappa_0           ; 16/04 for routine 'blanket' 
readf, unit, kappa_min         ; 16/04 for routine 'high_opacity'

readf,unit,strd
;READ IN SAVE_GRAPHS
readf,unit,save_graphs  
;--------
task=shape ; 14/04 IMPORTANTE!!

;CLOSE inputs.dat 
free_lun,unit ; close and deallocate the unit

; --------------------------------------
; READ PARAMETERS FROM INPUT FILE -- END
; --------------------------------------
switch_firsttime=1  ; this can serve later on as a switch to carry out certain 
                    ; operations the first time round and skip them later. 
; -------------------------
;  INITIAL CONDITIONS 
; -------------------------

initial ; sets up the initial condition initial returns the arrays of theprimitive variables density, velocity andpressure, i.e., rhoinit, vzinit, presinit
  
; -------------------------
;  INITIAL CONDITIONS - end   
; -------------------------
 
; ------------------------------------------------------------
; DECLARE / INITIALIZE VARIABLES JUST BEFORE STARTING THE LOOP ;06/03 variables de "initial.pro"
; ------------------------------------------------------------

temp=tempinit  & tempn = temp           ; inicializo la temperatura
flux=fltarr(n_elements(zz_flux))  & fluxn=flux          ;defino flujo; inicializo el flujo (npz-1) componentes

;---LOOP_VARIABLES--------------------

itt=0L   
time=0d0 

; --------------------------------------------------------
; INITIALIZE VARIABLES JUST BEFORE STARTING THE LOOP - end
; --------------------------------------------------------

; ---------------
; BIG LOOP BEGINS
; ---------------
while itt lt itmax and time lt timef do begin   
   
;   CALCULATE FLUXES FROM DENSITIES ('fluxes' is the name of the subroutine) 
    fluxes,temp,fluxn   ;CHANGE-.-.---.-.-.-.-.-.-.-.-.-.-.-.-.-.-.--.-.-.-.-

;   TIMESTEP 
    dt=cfl_new(kappa) ;10/04  ;CHANGE-.-.---.-.-.-.-.-.-.-.-.-.-.-.-.-.-.--.-.-.-.-

;   UPDATE ACROSS TIMESTEP using the chosen numerical scheme 
    update, dt,temp,flux,tempn,fluxn ;10/04   ;CHANGE-.-.---.-.-.-.-.-.-.-.-.-.-.-.-.-.-.--.-.-.-.-
    
;   BOUNDARY CONDITIONS ('bcs' below is the name of the routine)
    bcs,type_right,'right',tempn    ; type_right siempre 'fixed_temp'
    if type_left eq 'fixed_temp' then  bcs,type_left,'left',tempn    ;BIEN
    if type_left eq 'fixed_flux' then  bcs,type_left,'left',tempn   ;BIEN   
                
    		
;   EXCHANGE NEW AND OLD VARIABLES
    temp=tempn
    flux=fluxn
    ;rho = rhon & momz = momzn & energ = energn & pres = presn  & vz = vzn  ;CHANGE-.-.---.-.-.-.-.-.-.-.-.-.-.-.-.-.-.--.-.-.-.- 

    itt++      ; this an idl/C/C++ alternative to saying:    itt = itt + 1   
    time += dt ; this an idl/C/C++ alternative to saying:  time = time + dt


;   STORE RESULTS - to store results in a file from time to time
    if itt mod storecad eq 0 then begin
      save, filename='./SAVES/variables_itt'+string(itt)+'.sav'  ;09/03 NOTA: Creo carpeta SAVES para salvar variables, rutinas.. etc
      ;09/03 NOTA: Compressed SAVE files can be restored by the RESTORE procedure in exactly the same manner as any other SAVE file.
   endif

;   PLOT RESULTS 
    if itt mod plotcad eq 0 or itt eq 0 or time ge timef then begin
       draw, temp,flux,time,itt ;CHANGE-.-.---.-.-.-.-.-.-.-.-.-.-.-.-.-.-.--.-.-.-.-
    endif

endwhile

; --------------
; BIG LOOP - end
; --------------

print,'program finished.'+$
      '    itt= '+ strtrim(string(itt),2) +$
      '; time = '+ strtrim(string(time),2)
print,'-----------------end of program-------------------------'
end

;NOTES: 03/09 "undefine, a " por ejemplo, borra la variable a

