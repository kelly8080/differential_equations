
pro fluxes,temp,fluxn

; ----------------------------------------------------------------------
;
; ROUTINE FLUXES
;
;    PURPOSE: Calculate the fluxes f_m, f_c, f_e 
;
;    INPUT ARGUMENTS: the densities rho, momz and energ
;
;    OUTPUT:  the fluxes mflz, momflzz, energflz 
;
;    COMMON BLOCKS: Note that some necessary input is passed via common
;                       blocks (like the grid parameters and array zz, etc)
;
; ----------------------------------------------------------------------

@init_common
@grid_common
@general_common


;----------------FLUX--------------------------------------------------------------

   
  
;EXTREMO IZQ FIJO_FLUX-------> BUENO ;npz-1 componentes
fluxn[0:npz-2]= -(kappa[0:npz-2]+kappa[1:npz-1])/2d0 *(temp[1:npz-1]-temp[0:npz-2])/dz ;11/04

end

;INPUT kappa
;COMMON  temp


;NOTA: no importa conds de contorno aqui
