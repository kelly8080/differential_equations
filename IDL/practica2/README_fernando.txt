;PRACTICA 
;TECNICAS DE SIMULACION NUMERICA
;AUTOR: Alvaro Rioboo de Larriva
;FECHA DE ENTREGA: 17/04/2018

;-----------------------------------------README----------------------------------------------------------------------------------------------------------
; Consejos generales sobre la interpretacion del codigo:
;  	Esta bien comentado , los comentarios en espanol son mios,pone la fecha en la que se realizo.
;	La fecha mas reciente indica el valor mas actual de la sentencia que acompana el comentario. 
;       Hay comentarios con lenguaje bastante simple o llano. Pido disculpas por la falta de rigor que acompane en ciertas ocasiones.
;	Se han intentado obviar en comentarios y plots , simbolos como  , tildes y demas, por ventajas obvias de codificacion y para evitar problemas de compatibilidades.	       
;
;Proposito de README:
;	Este fichero da cuenta de los cambios que hay que realizar al programa para poder realizar de forma mas o menos sencilla las tareas que se piden en la practica.
;	Aunque en los programa ya queda bastante claro que comandos pertenecen a cada tarea segun los comentarios, se agradece una pequena recopilacion de los cambios.	
;CHECK-IN:
;Codigo: 
;	Ficheros .pro , .dat .  Detalle menor: se necesita una carpeta llamada "SAVES" en este directorio para que el "MAIN.pro" pueda guardar sus variables.También otra llamada "IMAGES"
; para que "draw.pro" pueda guardar los graficos
;
;
;Informe de la practica:		
;	
;	Practica2.pdf (PRACTICA 2:)
;
;Cambios realizados :
;    README_fernando.txt
;----------------------------------------------------------------------------------------------------------------------------------------------------------------------


------------------------------PRACTICA 2:-----------------------------------------------------------------------------------------
 THE HEAT CONDUCTION EQUATION
Las tareas se dividen en 4 casos que citar� a continuaci�n. Cada uno de ellos tiene su propia rutina de plot : "<shape>.pro" , a la que se llama desde "draw.pro" seg�n el shape que le asignemos en "inputs.dat".  
CASOS:

---- gaussian : perfil inicial gaussiano de temperatura , T fija a dos extremos (A,B)
"inputs.dat" Cambiar el primer argumento de las siguientes l�neas (comentadas en el mismo inputs)

0   +10                 za,zb
gaussian                shape
fixed_temp               (A)





---- parabola: perfil inicial parabola de temperatura , flujo fijo en (A)  ; T fija en (B)
"inputs.dat" Cambiar el primer argumento de las siguientes l�neas (comentadas en el mismo inputs)

-9  +7                 za,zb
parabola               shape
fixed_flux              (A)



---- blanket: perfil inicial rectilineo de temperatura afectado por una manta o material de poca conductividad,  T fijos a dos extremos (A,B)
"inputs.dat" Cambiar el primer argumento de las siguientes l�neas (comentadas en el mismo inputs)

-9  +7                  za,zb
blanket               shape
fixed_temp              (A)
0.05d0                   kappa_0;   (mas posibles cambios mencionados en "inputs.dat")





---- high_opacity: perfil inicial rectilineo de temperatura afectado por una regi�n de alta opacidad (poca conductividad) , flujo fijo en (A) ; T fija en (B)
"inputs.dat" Cambiar el primer argumento de las siguientes l�neas (comentadas en el mismo inputs)

-5   +7.5              za,zb
blanket              shape
fixed_temp            (A)
0.15d0                kappa_min ;  (mas posibles cambios mencionados en "inputs.dat")

                    


-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%
NOTA_1: En algunas ocasiones, es util cambiar los siguientes parametros en "inputs.dat" (comentados en "inputs.dat")
nint: numero de intervalos (resolucion). Se puede incrementar para estar seguros de que no hay efectos de difusion numerica (128L)
itt_max: iteracion maxima del bucle, si supera esta iteracion el bucle no sigue
itt_save: iteracion a la cual guardamos nuestros plots en la carpeta IMAGES/            (formato ".pgn")
timef: tiempo fisico maximo del bucle, si supera este tiempo el bucle no sigue          
plotcadence:  cadencia de los plots, hace que el programa vaya considerablemente mas rapido y se pierde poca fisica en comparacion.
fcl : parametro de courant, factor que depende de la escala de tiempos del sistema y nos marca el tiempo caracteristico de la evolucion en el programa.

save_graphs : elegir entre guardar los gr�ficos o no.


NOTA_2: Sobre im�genes, est�n todas en la carpeta IMAGES/<SHAPE>/     , se pueden ver las de una iteracion concreta haciendo , i.e.   eog *<iteracion>.png  (eog : programa de visualizacion)

NOTA_3: Variables guardadas en carpeta SAVES/

 
NOTA_4: Otras variables,como valores caracter�sticos de cada perfil de temperatura o conductividad, definidas en el propio "initial.pro"



----------------------------------------------------------------------------------------------------------------------------------------------------------------





