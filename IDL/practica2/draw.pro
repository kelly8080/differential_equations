PRO draw, temp,flux,time,itt
@init_common
@grid_common 
@general_common

  
device,decompose=0
!p.background=255
!p.color=0
;!p.charsize=1.1
loadct,39,/silent

;----------------VARIABLES DE CONTROL---------------------------------------

;task (VARIABLE, "inputs.dat")
plot_cadence = 400

;WINDOWS------------------------
if switch_firsttime then begin  ; truco para hacer algo solo una vez
  device,window_state=wstat            ;--> no destruir ventanas
  if not(wstat[0]) then window, xs=800,ys=250, xp=900,yp=750,tit='Temperature (0)'  ,0  ;;TEMPERATURE
  if not(wstat[1]) then window, xs=800,ys=250, xp=900,yp=400,tit='Flujo de calor (1)',1  ;;FLUJO DE CALOR
  if not(wstat[2]) then window, xs=800,ys=250, xp=900,yp=0,tit='Conductividad (2)' ,2  ;;CONDUCTIVIDAD
  switch_firsttime = 0 ;no repetir
endif

;TASKS--------------------------------
if task eq 'gaussian' then gaussian   ,temp,flux,time,itt 
if task eq 'parabola'     then parabola    ,temp,flux,time,itt
if task eq 'blanket'      then blanket     ,temp,flux,time,itt
if task eq 'high_opacity' then high_opacity,temp,flux,time,itt
end



;checkk='gaussian' or 'parabola' or 'blanket' or 'high_opacity'
;if checkk then general_draw,temp,time,itt

;NOTAS ;11/03 Simbolos Latex: !3 es alfabeto alfanumerico. !4 alfabeto griego (ir probando) ;!u : upper_script , !d : down_script , !n : normal level

 
