PRO INITIAL

; ----------------------------------------------------------------------
; ROUTINE INITIAL
;
;    PURPOSE:  Calculate the arrays of density, velocity and pressure 
;                 at time t=0. 
;    INPUT ARGUMENTS: they are passed via common block init_common
;              inittype:   char variable with the choice of initial condition
;                 shape:   char variable with some subsidiary choice
;                 
;	** please fill in the list with your choices **
;            
;    COMMON BLOCKS: Note that some additional necessary input is passed via
;                   other common blocks (like the grid parameters,  etc)
;
;    OUTPUT:  the arrays rhoinit, vzinit, presinit (common block)
; ----------------------------------------------------------------------

@grid_common
@init_common
@general_common
;--------------------------------------------------------
if inittype eq 'heat_conduction' then begin
    if shape eq 'gaussian' then begin    ;09/04 TASK 1, constant conductivity and b.c.'s with fixed temperature
    ;B.C.'s                           
    T0=1.2d0                    ;Temperature of both points
    Ta=T0  &  Tb=T0             ;left & right   Physical temperatures
    ;PARAMETERS
    W0=1d0                      ; FWHM
    AA=2.3d0                    ; cte. propto exp.
    ;CONDUCTIVITY
    val_kappa=0.9d0    &         kappa=fltarr(npz)+val_kappa ; make_array(npz) tb funciona
    ;VALUES_GRID ;za=0d0 & zb=10d0 definidos en "inputs.dat"
    zc=zz[round(npz/2d0)]       ; centered point (gaussian). ELEGIR!
    ;TEMPERATURE_INITIAL
    tempinit  =  T0 * ( 1 + AA  *  exp(- (  (zz-zc)^2d0/W0^2d0 ) ) )
    diff_temp= -2d0*T0* AA/W0^2d0 *(zz-zc) *exp( - ((zz-zc)/W0)^2d0)
   
 endif

 if shape eq 'parabola' then begin  ;11/04 TASK 2 , uniform kappa and mixed b.c.'s
    ;B.C.'s
    fa=0.7d0      &     Tb=1d0  ; left & right Physical flux , temperature
    ;PARAMETERS
    ;-------------------
    ;CONDUCTIVITY
    val_kappa=0.9d0   &    kappa=fltarr(npz)+val_kappa ; make_array(npz) tb funciona
    ;VALUES_GRID ; za=-9d0     &       zb=7d0  ;("inputs.dat")
    ;TEMPERATURE_INITIAL
    tempinit=(fa/kappa[0])/(1.4d0*(zb-za)) * ((zz-za)^2d0 - (zb-za)^2d0) - (fa/kappa[0])* (zz-zb) + Tb
    diff_temp = (fa/kappa[0])/(1.4d0*(zb-za))*2d0*(zz-za)-(fa/kappa[0])
 endif

 if shape eq 'blanket' then begin
    ;B.C.'s
    Ta=2.0d0       &    Tb=1.d0   ; left & right Physical temperatures
    ;PARAMETERS
    A1=1d0         &     A2=2d0 
    W1 = 0.3d0     &     W2=W1
    z1=-2d0        &     z2=2d0 ; extensi�n de la manta ('blanket')
    zc=0        ; no hace falta, pero se queja
    ;CONDUCTIVITY----------------------------------- (;16/04 kappa_0 definido en ("inputs.dat"))    
    kappa  =  kappa_0 + A1  *( 1d0 + tanh ((z1-zz)/W1))+ A2 *(1d0 + tanh((zz-z2)/W2)) 
    ;TEMPERATURE_INITIAL 
    dTdz  =  (Tb-Ta)/(zb-za)    ; < 0     ;11/04 ?? straight profile
    tempinit  =   - dTdz*dz*reverse(dindgen(npz)) +   (Tb+dTdz*dz/2d0) ; ??   ;use for T(z) a decresing (LINKING BOUNDARIES)
    diff_kappa= A2*cosh((zz-zc)/W2)^(-2d0)/W2  -  A1*cosh((z1-zz)/W1)^(-2d0)/W1   
 endif

 if shape eq 'high_opacity' then begin
    ;B.C.'s
    fa=0.7d0      &     Tb=1d0       ; left & right Physical temperatures
    ;PARAMETERS--------------------------------------(;16/04 kappa_min definido en ("inputs.dat)) 
    delta       = 0.35d0
    kappa_infty = 1.1d0         
    z1=-2.0d0                 &    z2=3.0d0 ;extensi�n de la manta('blanket')
    zc=0        ; no hace falta, pero se queja
    ;CONDUCTIVITY
    kappa = kappa_infty  +  (kappa_infty - kappa_min)* (1 - (1/2d0)*(1+tanh((zz-z1)/delta))+(1/2d0)*(1+tanh((zz-z2)/delta)))
    ;TEMPERATURE_INITIAL 
    dTdz=  -  0.5d0    ; < 0    ; 
    tempinit =  - dTdz*dz*reverse(dindgen(npz)) + (Tb+dTdz*dz/2d0) ;??  ;straight profile?
    diff_kappa = (kappa_infty - kappa_min)* (cosh((zz-z2)/delta)^(-2d0)/(2*delta) - cosh( (zz-z1)/delta)^(-2d0)/(2*delta))
 endif
endif

end






;NOTES Tb se puede considerar fijo en los 4 casos.
