
pro fluxes,rho,momz,energ,     mflz,momflzz,energflz

; ----------------------------------------------------------------------
;
; ROUTINE FLUXES
;
;    PURPOSE: Calculate the fluxes f_m, f_c, f_e 
;
;    INPUT ARGUMENTS: the densities rho, momz and energ
;
;    OUTPUT:  the fluxes mflz, momflzz, energflz 
;
;    COMMON BLOCKS: Note that some necessary input is passed via common
;                       blocks (like the grid parameters and array zz, etc)
;
; ----------------------------------------------------------------------



  dens2prim, rho,momz,energ,vz,pres ; this calculates vz,pres

  mflz = momz   ; mass flux ;10/03 el flujo de masa es el momento.;10/03 BIEN
  momflzz = momz*momz/rho + pres   ; momentum flux ;10/03 BIEN
  energflz = (energ + pres) * vz   ; energy flux  ;10/03 no veo esta formula ;10/03 energ =rho*epsilon +momz*momz/rho ;10/03 BIEN
	
end
