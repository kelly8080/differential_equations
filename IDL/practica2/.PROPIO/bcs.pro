PRO BCS,type,site,varr

; ----------------------------------------------------------------------
; ROUTINE BCS
;
;    PURPOSE:  Calculate the boundary conditions of a variable
;    INPUT ARGUMENTS:  PLEASE, FILL THIS IN
;		site
;		
;    COMMON BLOCKS: Note that some necessary input is passed via common
;                       blocks (like the grid parameters and array zz, etc)
;
;    OUTPUT:  PLEASE, FILL THIS IN
; ----------------------------------------------------------------------


@grid_common


; INITIAL CHECK: type of boundary condition and site
checkv = type ne 'periodic' or (site ne 'left' and site ne 'right') ;Chequea TRUE or FALSE si es 'periodic' o ..



;-------------CONTOUR_CONDITIONS (si checkv='TRUE')------- ;08/03 Tenemos que hacer que [0]=[npz-2] y [npz-1]=1 ;10/03 para LAX_FRIEDRICH de momento


if checkv then print,'bcs.pro: bc type or site not included'    ;08/03 !ESTA SENTENCE NO NECESITA ENDIF! ;10/03 si es verdad que no es periódica,"di" que no está incluido el esquema
; the if-clause for type is introduced for possible future extensions

;-----------LAX_FRIEDRICH---------------------------------------------------------

if type eq 'periodic' then if site eq 'left'  then varr[0] = varr[npz-2]

if type eq 'periodic' then if site eq 'right' then varr[npz-1] =  varr[1]

;------------END LAX_FRIEDRICH---------------------------------------------





end

