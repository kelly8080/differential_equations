
; programa que ejemplifica cómo crear animaciones


set_plot,'Z'
device,decomposed=0
device,set_pixel_depth=24 

; define the defaults (background color, character size, etc) for the movie
 white=255 & black=0
!p.background=white &    !p.color=black
!p.charsize=1.8 & !x.charsize=1.2 & !y.charsize=1.2
!p.thick=3 & !x.thick=3 & !y.thick=3 & !p.charthick=3
loadct,39 

; define the pixel size of the movie frame
width = 800   & height = 500   
device,set_resolution=[width,height]

fps = 6                         ; frames per second in the resulting movie

  ; input the name of the file to hold the movie at the end of the program
videofile='video.mp4'
videom = IDLffVideoWrite(videofile) ;, FORMAT='mp4')

; sets the size of the snapshots and the frame-per-second rate
vidStream = videom.AddVideoStream(width, height, fps)


; actual plotting: 20 snapshots foreseen
np=258
x0 = 0. & xf=1.
xx = findgen(np)/(np-1)*(xf-x0) + x0

i0 = 0
ifin=20
for ii = i0, ifin do begin

    yy = (xx-(xf-x0)*ii / ifin)^2d0 ; note, by the way,  that the ratio 
                                    ; of integers is not a problem here. 

    plot,xx,yy,xr=[x0,xf],/xst,yr=[0,1],/yst,xtit='x',ytit='y'

    !null = videom.Put(vidStream, TVRD(TRUE=1))
    if ii mod 10 eq 0 then print,ii
endfor

; videom = 0
videom.cleanup
print,'video file '+videofile+' created.'


; back to the standard window plotting (also redefining the default sizes, etc)

set_plot,'x'
!p.background=white &    !p.color=black
!p.charsize=1.8 & !x.charsize=1.2 & !y.charsize=1.2
!p.thick=3 & !x.thick=3 & !y.thick=3 & !p.charthick=3
plot,xx,yy,xr=[x0,xf],/xst,yr=[0,1],/yst,xtit='x',ytit='y'

end
