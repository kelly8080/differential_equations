FUNCTION cfl_new, kappa


; ------------------------------------------------------------------------------------------------;
;												;
; FUNCTION CFL											;
;												;	
;    PURPOSE: Calculate the timestep to guarantee numerical stability				;		
;												;
;    INPUT ARGUMENTS: those necessary to calculate the sound speed, namely			;
;           rho, momz, pres									;
; 												;	
;    COMMON BLOCKS: Note that some necessary input is passed via common				;
;                       blocks (like gamm or the grid zz)					;
;    OUTPUT:  the delta t 									;
; 												;
; ----------------------------------------------------------------------------------------------;

@general_common.pro
@grid_common.pro

;CALCULA "dt" POR LA CONDICION DE COURANT (parametro de Courant cfl)y OTRO criterio seg�n nuestro esquema num�rico


dt = cfl*  (dz)^2d0 / max(kappa) ;10/04 Escoger el m�ximo de kappa (vector conductividad t�rmica)

return,dt

end
;COMMON   cfl
;INPUT kappa
