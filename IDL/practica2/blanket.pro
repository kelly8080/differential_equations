pro blanket,temp,flux,time,itt             ;,vz,rho,pres, itt,time  ;14/03
@grid_common
@init_common
@general_common

	;---------------------------PLOT_1:TEMPERATURE----------------------
	wset,0  ; selecciona la ventana 0
        ;PLOTS_
        plot,zz,temp,/nodata,xr=[za,zb],yr=[Tb,Ta],/xst,$
             tit='Temperature Evolution',xtit='z',ytit='T(z)'  
        oplot,zz,temp,thick=3,col=250   ;SOLUCION NUMERICA ; ROJO!
        oplot,zz,tempinit,thick=2.5,psym=3,col=200 ;SOLUCION ANALITICA; 

        ;Maximo y su posicion
	max_temp=max(temp)      &    arg_max_temp=where(temp eq max_temp)  
	
	;SPECIFICS
	plot_specifics, itt,time  

        ;---------------------------PLOT_2:FLUX-----------------------------------
	wset,1  
	;PLOTS_
        plot,zz_flux,flux,/nodata,xr=[za,zb],/xst,yr=[1e-5,1e1],/ylog,$
             tit='Diffusive Flux Evolution',xtit='z',ytit='flux(z)' 
        oplot,zz_flux,flux,thick=3,col=200            ;SOLUCION NUMERICA; NEGRO!
	;SPECIFICS
	plot_specifics, itt,time  

	;-------------------------PLOT_3:CONDUCTIVITY---------------------------------
	wset,2
	;PLOTS_
        plot,zz,kappa,/nodata,xr=[za,zb],yr=[0,6],/xst,$
             tit='Conductivity Evolution', xtit='z',ytit='!4j !3(z)' ; !4j corresponde a kappa
        oplot,zz,kappa,thick=3,col=300   ;SOLUCION NUMERICA; NEGRO!
	;SPECIFICS 
        plot_specifics, itt,time
        
                                
        ;-------------------------------------------------------------------
	;OTHERS
        folder='./IMAGES/BLANKET/'
        itt_save_str= strtrim(string(round(itt_save)),2) ; string de la iteracion
        format='.png'
	;SAVE_FIG
        if itt eq itt_save and save_graphs then begin
                wset,0
                filename0 = folder +'temperature'+'_blanket_' + itt_save_str + format 
                write_png, filename0,tvrd(/TRUE)
                
                wset,1
           	filename1 = folder +'flux'+'_blanket_'+itt_save_str + format
                write_png, filename1, tvrd(/TRUE)
                
                wset,2
		filename2 = folder +'conductivity'+'_blanket_'+itt_save_str + format 
		write_png, filename2,tvrd(/TRUE)
	endif

	;WAIT----------------------------------------------------------------------------------------
	wait,0.001  ;changes: 0.001>0.01 
	
end
