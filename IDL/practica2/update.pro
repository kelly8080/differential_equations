pro update, dt, temp,flux,tempn,fluxn


; ----------------------------------------------------------------------
; ROUTINE update
;
;    PURPOSE:  Calculate variables at timestep n+1. This routines contains
;              the numerical scheme used in the code. 
;
;    INPUT ARGUMENTS:  - the densities at timestep n, namely rho, momz, energ
;                      - the fluxes mflz, momflzz, energflz
;
;    COMMON BLOCKS: Note that some necessary input is passed via common
;                       blocks (like the grid parameters and array zz, etc)
;
;    OUTPUT:  the densities at timestep n+1, namely rhon, momzn, energn
; ----------------------------------------------------------------------


@general_common.pro
@grid_common


lambda =  dt / dz  ;10/03 (=DELTAt/DELTAx)   ; "dz" pasada por common, "dt" por nuestro procedure 



;-----------------------SPATIALLY CENTERED FORWARD IN TIME : FTCS---------------------------------
tempn[1:npz-2]=temp[1:npz-2]-lambda*(fluxn[1:npz-2]-fluxn[0:npz-3]) ;10/04 lo que se refiere uu o u es temp en el bucle

; IMPORTANT NOTE: no boundary conditions should be applied here!
;-----------------------------------------------------------------------------------------------------------





end

