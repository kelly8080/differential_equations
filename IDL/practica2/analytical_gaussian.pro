function analytical_gaussian,time

@grid_common

AAA=2.3d0
kappa2=0.9d0
W00=1d0
T00=1.2d0
zcc=5d0

WW= sqrt(W00^(2d0)+4*kappa2[0]*time)                            ; necesito TIME



TT= T00+ T00*(AAA/(WW/W00)) * exp(-(zz-zcc)^(2d0)/WW^(2d0) )  ;BIEN

return, TT

end
