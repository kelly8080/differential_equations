pro bcs ,type,site,varr

@init_common
@grid_common
@general_common
  
; INITIAL CHECK: type of boundary condition and site
checkv = (type eq 'fixed_temp' or type eq 'fixed_flux') and (site eq 'left' or site eq 'right') ;Chequea TRUE or FALSE si es 'heat_conduction' ;10/04


;-------------CONTOUR_CONDITIONS (si checkv='TRUE')------- 
if not checkv then print,'bcs.pro: bc type or site not included'   ;10/04

;-----------1st TYPE: FIXED_TEMPERATURE--------------------------------

if type eq 'fixed_temp' then if site eq 'left'  then varr[0]     =  2d0*Ta-varr[1]

if type eq 'fixed_temp' then if site eq 'right' then varr[npz-1] =  2d0*Tb-varr[npz-2]

;------------2nd TYPE: FIXED_FLUX -------------------------------------------------
if type eq 'fixed_flux' then if site eq 'left'  then varr[0]    = + (2d0*dz*fa)/(kappa[0]+kappa[1])+varr[1]

if type eq 'fixed_flux' then if site eq 'right' then varr[npz-1]= - (2d0*dz*fb)/(kappa[npz-2]+kappa[npz-1])+varr[npz-2]

end











;CHANGES
;fixed_temp:  ;left         varr[0]=2*Ta-varr[1]
              ;right        varr[npz-1]=2*Tb-varr[npz-2] 

;fixed_flux:  ;left         varr[0]    = +(2*dz*fa)/(kappa[0]+kappa[1])+varr[1]                
              ;right        varr[npz-1]= -(2*dz*fb)/(kappa[npz-2]+kappa[npz-1])+varr[npz-2]
;NOTES--------; tener definidas :
;INPUT:   kappa[0:npz-1] , varr[1:npz-2],                  type,site
;COMMON:  dz,Ta,Tb,fa,fb                                  
