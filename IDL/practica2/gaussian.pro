pro gaussian,temp,flux,time,itt             ;,vz,rho,pres, itt,time  ;14/03
@grid_common
@init_common
@general_common


        T0=1d0                        ;factor de plots
        TT=analytical_gaussian(time)
        device,window_state=wstat
        ;---------------------------PLOT_1:TEMPERATURE----------------------
        wset,0                 ; selecciona la ventana 0
        ;PLOTS_--------------------------------------------
        plot,zz,temp,/nodata,xr=[za,zb],/xst,yr=[T0,4*T0],$
             tit='Temperature Evolution',xtit='z',ytit='T(z)'  
        oplot,zz,temp,thick=3,col=250   ;SOLUCION NUMERICA ; ROJO!
        oplot,zz,tempinit,thick=2.5,psym=3,col=500 ;CONDICION INICIAL!

        plot_specifics,itt,time
        ;Maximo y su posicion
        max_temp=max(temp)      &    arg_max_temp=where(temp eq max_temp)

        ;ANALYTIC
        oplot,zz,TT,thick=1.5,psym=2,col=600 ; SOLUCION ANALITICA!!
       
        if not(wstat[3]) then window, xs=800,ys=250, xp=100,yp=750,tit='Desviacion relativa (3)' ,3  
        wset,3
        plot,zz,temp,/nodata,xr=[za,zb],/xst,yr=[-1,1],$
             tit='Relative deviation',xtit='z',ytit='T(z)'  
        oplot,zz,abs((TT-temp)/TT),$
              thick=3,line=0,col=800 ;DESVIACION RELATIVA
        
        
        
	;SPECIFICS
	plot_specifics, itt,time  

        ;---------------------------PLOT_2:FLUX-----------------------------------
	wset,1  
	;PLOTS_
        plot,zz_flux,flux,/nodata,xr=[za,zb],/xst,yr=[-2,2],$
             tit='Diffusive Flux Evolution',xtit='z',ytit='flux(z)'
        oplot,zz_flux,flux,thick=3,col=200            ;SOLUCION NUMERICA; NEGRO!
	;SPECIFICS
	plot_specifics, itt,time  

	;-------------------------PLOT_3:CONDUCTIVITY---------------------------------
	wset,2
	;PLOTS_
        plot,zz,kappa,/nodata,xr=[za,zb],/xst,yr=[0,2],$
             tit='Conductivity Evolution', xtit='z',ytit='!4j!3(z)' ; !4j corresponde a kappa
        oplot,zz,kappa,thick=3,col=300   ;SOLUCION NUMERICA; NEGRO!
	;SPECIFICS 
        plot_specifics, itt,time
        
        ;----------------------------------------------------------------------------
        ;OTHERS
        folder='./IMAGES/GAUSSIAN/'     ;carpeta
        itt_save_str= strtrim(string(round(itt_save)),2)  ;string de la iteracion
        format='.png'                   ;formato de guardado
	;SAVE_FIG
        if itt eq itt_save and save_graphs then begin
                wset,0
		filename0 = folder +'temperature'+'_gaussian_'+itt_save_str+format 
                write_png, filename0,tvrd(/TRUE)
                
                wset,1
		filename1 = folder +'flux'+'_gaussian_'+itt_save_str+format 
		write_png, filename1, tvrd(/TRUE)

                wset,2
		filename2 = folder +'conductivity'+'_gaussian_'+itt_save_str + format 
                write_png, filename2,tvrd(/TRUE)

                wset,3
                filename3 = folder + 'relativedist'+'_gaussian_'+itt_save_str+format
                write_png, filename3,tvrd(/TRUE)
	endif

	;WAIT----------------------------------------------------------------------------------------
	wait,0.01  ;changes: 0.001>0.01 
	
end
