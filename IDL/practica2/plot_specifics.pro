PRO PLOT_SPECIFICS,itt,time
@grid_common

oplot,zz,dblarr(n_elements(zz)),thick=2,linestyle=1 ;11/04

itt_form  =  string(itt, FORMAT='(I7)')     ;11/04
time_form =  string(time, FORMAT='(F7.3)')  ;11/04

xyouts,/norm,0.9,0.91,col=600,'time='+ time_form  ;11/04 tiempo transcurrido
xyouts,/norm,0.9,0.96,col=500,'itt='+ itt_form    ;11/04 numero iteraciones 

end
